﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StudentManagement.DataLayer.Entity;
using StudentManagement.DataLayer.Entity.StudentDTO;

namespace StudentManagement.BusinessLayer.IRepository
{
    public interface IStudentRepository
    {
        Task<IEnumerable<Student>> GetStudents();
        Task<IEnumerable<object>> GetStudentsWithAddress();
        Task<IEnumerable<object>> GetStudentsWithAddressBooks();
        Task<IEnumerable<object>> GetStudentsWithAddressBooksCourses();
        Task<Student> GetStudentByID(Guid studentID);
        Task<object> GetStudentInformationByID (Guid studentID);
        Task<IEnumerable<object>> GetStudentWithNumberCourse();
        Task<Student> AddStudent(StudentDTO student);
        Task<StudentCourse> AddRegistration(StudentCourseDTO studentCourse);
        Task<Student> UpdateStudent(StudentDTO student);
        Task<Student> DeleteStudent (Guid studentID);
        
    }
}
