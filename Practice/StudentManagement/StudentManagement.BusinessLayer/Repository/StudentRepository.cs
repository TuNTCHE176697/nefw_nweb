﻿using Microsoft.EntityFrameworkCore;
using StudentManagement.BusinessLayer.IRepository;
using StudentManagement.DataLayer.Context;
using StudentManagement.DataLayer.Entity;
using StudentManagement.DataLayer.Entity.StudentDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.BusinessLayer.Repository
{
    public class StudentRepository : IStudentRepository
    {
        protected readonly StudentManagementContext _context;

        public StudentRepository(StudentManagementContext context)
        {
            _context = context;
        }

        public async Task<StudentCourse> AddRegistration(StudentCourseDTO studentCourse)
        {
            var registration = await _context.StudentCourses
                                     .Where(sc => sc.CourseID == studentCourse.CourseID && sc.StudentID == studentCourse.StudentID)
                                     .FirstOrDefaultAsync();
            var student = await _context.Students.SingleOrDefaultAsync(x => x.StudentID == studentCourse.StudentID);
            var course = await _context.Courses.SingleOrDefaultAsync(x => x.CourseID == studentCourse.CourseID);

            if(student == null || course == null)
            {
                return null;
            }    

            if (registration == null)
            {
                var RegistrationCredit = new StudentCourse
                {
                    StudentID = studentCourse.StudentID,
                    CourseID = studentCourse.CourseID,
                    Student = _context.Students.FirstOrDefault(x => x.StudentID ==  studentCourse.StudentID),
                    Course = _context.Courses.FirstOrDefault(x => x.CourseID == studentCourse.CourseID)
                };
                var result = _context.StudentCourses.Add(RegistrationCredit);
                await _context.SaveChangesAsync();
                return result.Entity;
            }

            return null;
        }

        public async Task<Student> AddStudent(StudentDTO student)
        {
            var studentCredit = new Student
            {
                StudentID = student.StudentID,
                StudentName = student.StudentName,
                StudentAdress = new StudentAdress
                {
                    Address1 = student.StudentAddress.Address1,
                    Address2 = student.StudentAddress.Address2,
                },
            };
            studentCredit.Books = new List<Book>();
            foreach (var book in student.Books)
            {
                var book1 = new Book();
                book1.BookID = book.BookID;
                book1.BookName = book.BookName;
                studentCredit.Books.Add(book1);
            }
            var result = await _context.Students.AddAsync(studentCredit);
            await _context.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<Student> GetStudentByID(Guid studentID)
        {
            return await _context.Students.FirstOrDefaultAsync(x => x.StudentID == studentID);
        }

        public async Task<object> GetStudentInformationByID(Guid studentID)
        {

            var student = await _context.Students
                           .Join(_context.StudentAdresses,
                                 student => student.StudentID,
                                 address => address.StudentID,
                                 (student, address) => new { student, address })
                           .Join(_context.Books,
                                 student1 => student1.student.StudentID,
                                 book => book.StudentID,
                                 (student1, book) => new
                                 {
                                     StudentID = student1.student.StudentID,
                                     StudentName = student1.student.StudentName,
                                     Address1 = student1.address.Address1,
                                     Address2 = student1.address.Address2,
                                     BookName = book.BookName,
                                 }
                                 )
                           .FirstOrDefaultAsync(x => x.StudentID == studentID);
            return student;

        }

        public async Task<IEnumerable<Student>> GetStudents()
        {
            return await _context.Students.ToListAsync();
        }


        public async Task<IEnumerable<object>> GetStudentsWithAddress()
        {
            var students = await _context.Students
                           .Join(_context.StudentAdresses,
                                 student => student.StudentID,
                                 address => address.StudentID,
                                 (student, address) => new
                                 {
                                     StudentID = student.StudentID,
                                     StudentName = student.StudentName,
                                     Address1 = address.Address1,
                                     Address2 = address.Address2,
                                 })
                           .ToListAsync();
            return students;
        }

        public async Task<IEnumerable<object>> GetStudentsWithAddressBooks()
        {
            var students = await _context.Students
                           .Join(_context.StudentAdresses,
                                 student => student.StudentID,
                                 address => address.StudentID,
                                 (student, address) => new { student, address })
                           .Join(_context.Books,
                                 student1 => student1.student.StudentID,
                                 book => book.StudentID,
                                 (student1, book) => new
                                 {
                                     StudentID = student1.student.StudentID,
                                     StudentName = student1.student.StudentName,
                                     Address1 = student1.address.Address1,
                                     Address2 = student1.address.Address2,
                                     BookName = book.BookName
                                 })
                           .ToListAsync();
            return students;
        }
        public async Task<IEnumerable<object>> GetStudentsWithAddressBooksCourses()
        {
            var students = await _context.Students
                           .Join(_context.StudentAdresses,
                                 student => student.StudentID,
                                 address => address.StudentID,
                                 (student, address) => new { student, address })
                           .Join(_context.Books,
                                 student1 => student1.student.StudentID,
                                 book => book.StudentID,
                                 (student1, book) => new { student1, book })
                           .Join(_context.StudentCourses,
                                 student2 => student2.student1.student.StudentID,
                                 studentcourse => studentcourse.StudentID,
                                 (student2, studentcourse) => new { student2, studentcourse })
                           .Join(_context.Courses,
                                 student3 => student3.studentcourse.CourseID,
                                 course => course.CourseID,
                                 (student3, course) => new
                                 {
                                     StudentID = student3.student2.student1.student.StudentID,
                                     StudentName = student3.student2.student1.student.StudentName,
                                     Address1 = student3.student2.student1.address.Address1,
                                     Address2 = student3.student2.student1.address.Address2,
                                     BookName = student3.student2.book.BookName,
                                     CourseName = course.CourseName
                                 }
                                 )
                           .ToListAsync();
            return students;
        }

        public async Task<IEnumerable<object>> GetStudentWithNumberCourse()
        {
            var students = await _context.Students
                                .GroupJoin(_context.StudentCourses,
                                          student => student.StudentID,
                                          studentCourse => studentCourse.StudentID,
                                         (student, studentCourse) => new
                                         {
                                             StudentId = student.StudentID,
                                             StudentName = student.StudentName,
                                             CourseCount = studentCourse.Count()
                                         }
                                         )
                                .ToListAsync();
            return students;
        }

        public async Task<Student> UpdateStudent(StudentDTO student)
        {
            var studentResult = await _context.Students.Include(x => x.StudentAdress).FirstOrDefaultAsync(x => x.StudentID == student.StudentID);

            if (studentResult != null)
            {
                studentResult.StudentID = student.StudentID;
                studentResult.StudentName = student.StudentName;
                studentResult.StudentAdress.Address1 = student.StudentAddress.Address1;
                studentResult.StudentAdress.Address2 = student.StudentAddress.Address2;
                studentResult.Books = new List<Book>();
                foreach(var b in student.Books)
                {
                    Book book = new Book();
                    book.BookID = b.BookID;
                    book.BookName = b.BookName;
                    studentResult.Books.Add(book);
                }
                await _context.SaveChangesAsync();
                return studentResult;
            }
            return null;
        }

        public async Task<Student> DeleteStudent(Guid studentID)
        {
            var studentResult = await _context.Students.FirstOrDefaultAsync(x => x.StudentID == studentID);
            
            if (studentResult != null)
            {
                _context.Students.Remove(studentResult);
                await _context.SaveChangesAsync();
                return studentResult;
            }
            return null;
        }



    }
}
