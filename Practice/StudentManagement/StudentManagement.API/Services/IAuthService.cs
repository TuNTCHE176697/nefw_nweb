﻿using StudentManagement.DataLayer.Entity;

namespace StudentManagement.API.Services
{
    public interface IAuthService
    {
        string GenerateTokenString(LoginUser loginUser);
        Task<bool> RegisterUser(LoginUser user);
        Task<bool> Login(LoginUser user);
    }
}