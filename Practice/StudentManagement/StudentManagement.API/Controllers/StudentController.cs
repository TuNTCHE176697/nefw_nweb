﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StudentManagement.BusinessLayer.IRepository;
using StudentManagement.DataLayer.Entity;
using StudentManagement.DataLayer.Entity.StudentDTO;

namespace StudentManagement.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class StudentController : ControllerBase
    {
        private readonly IStudentRepository _studentRepository;

        public StudentController(IStudentRepository studentRepository)
        {
            _studentRepository = studentRepository;
        }

        [HttpGet]
        [ProducesResponseType(200, Type = typeof(IEnumerable<Student>))]
        public async Task<ActionResult<IEnumerable<Student>>> GetStudents()
        {
            try
            {
                var students = await _studentRepository.GetStudents();

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                return Ok(students);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }
            

        }

      
        [HttpGet]
        [ProducesResponseType(200, Type=typeof(IEnumerable<object>))]
        public async Task<ActionResult<IEnumerable<object>>> GetStudentsWithAddress()
        {
            try
            {
                var students = await _studentRepository.GetStudentsWithAddress();
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(students);
            }
            catch (Exception e)
            {
               
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }
        }
        
   
        [HttpGet]
        [ProducesResponseType(200, Type=typeof(IEnumerable<object>))]
        public async Task<ActionResult<IEnumerable<object>>> GetStudentsWithAddressBook()
        {
            try
            {
                var students = await _studentRepository.GetStudentsWithAddressBooks();
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(students);
            }
            catch (Exception e)
            {
               
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }
        }

    
        [HttpGet]
        [ProducesResponseType(200, Type=typeof(IEnumerable<object>))]
        public async Task<ActionResult<IEnumerable<object>>> GetStudentsWithAddressBookCourse()
        {
            try
            {
                var students = await _studentRepository.GetStudentsWithAddressBooksCourses();
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(students);
            }
            catch (Exception e)
            {
               
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }
        }
        [HttpGet]
        [ProducesResponseType(200, Type=typeof(IEnumerable<object>))]
        public async Task<ActionResult<IEnumerable<object>>> GetStudentWithNumberOfCourse()
        {
            try
            {
                var students = await _studentRepository.GetStudentWithNumberCourse();
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(students);
            }
            catch (Exception e)
            {
               
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }
        }

        [HttpGet("{studentID:Guid}")]
        [ProducesResponseType(200, Type = typeof(Student))]
        [ProducesResponseType(400)]
        public async Task<ActionResult<Student>> GetStudentByID(Guid studentID)
        {
            try
            {
                var student = await _studentRepository.GetStudentByID(studentID);
                if (student == null)
                {
                    return NotFound();
                }
                if(!ModelState.IsValid)
                    return BadRequest(ModelState);

                return Ok(student);
            }
            catch (Exception e)
            {

                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }
        }


        [HttpGet("{studentID:Guid}")]
        [ProducesResponseType(200, Type = typeof(object))]
        [ProducesResponseType(400)]
        public async Task<ActionResult<object>> GetStudentInformationByID(Guid studentID)
        {
            try
            {
                var student = await _studentRepository.GetStudentInformationByID(studentID);
                if (student == null)
                {
                    return NotFound();
                }
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
                return Ok(student);
            }
            catch (Exception e)
            {

                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }
        }

      
        [HttpPost]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<Student>> CreateStudent(StudentDTO student)
        {
            try
            {
                if (student == null)
                {
                    return BadRequest(ModelState);
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var createdStudent = await _studentRepository.AddStudent(student);

                return Ok(student);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error creating Student");
            }
        }
       
        [HttpPost]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<StudentCourse>> AddRegistration(StudentCourseDTO studentcourse)
        {
            try
            {
                if (studentcourse == null)
                {
                    return BadRequest(ModelState);
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var createdRegistration = await _studentRepository.AddRegistration(studentcourse);
                if(createdRegistration == null)
                {
                    return BadRequest("Not exist StudentID or CourseID or Duplicate registration");
                }    
                return Ok(studentcourse);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error creating Rgistration");
            }
        }

        [HttpPut("{id:Guid}")]
        [ProducesResponseType(204)]
        [ProducesResponseType(400)]
        public async Task<ActionResult<Student>> UpdateProduct(Guid id, StudentDTO student)
        {
            try
            {
                if (id != student.StudentID)
                    return BadRequest("Student ID mismatch");

                var studentUpdate = await _studentRepository.GetStudentByID(id);

                if (studentUpdate == null)
                    return NotFound($"Student with Id = {id} not found");

                var result =  await _studentRepository.UpdateStudent(student);
                if(result == null)
                {
                    return BadRequest("StudentID mismatch");

                }    
                else return Ok(student);

            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error updating data");
            }
        }

        [HttpDelete("{studentID:Guid}")]
        public async Task<ActionResult<Student>> DeleteStudent(Guid studentID)
        {
            try
            {
                var studentDelete = await _studentRepository.GetStudentByID(studentID);

                if (studentDelete == null)
                {
                    return NotFound($"Student with Id = {studentID} not found");
                }

                var studentResult = await _studentRepository.DeleteStudent(studentID);
                if (studentResult == null)
                {
                    return BadRequest("StudentID mismatch");

                }
                else return Ok(studentResult);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error deleting data");
            }
        }
    }
}
