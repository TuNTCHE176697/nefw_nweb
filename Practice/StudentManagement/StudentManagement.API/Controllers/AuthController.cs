﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using StudentManagement.API.Services;
using StudentManagement.DataLayer.Entity;

namespace StudentManagement.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthService _authService;
        public AuthController(IAuthService authService) 
        { 
            _authService = authService;
        }
    
            public IActionResult Index()
            {
                // Xử lý yêu cầu GET theo quy tắc mặc định
                return View("sdcsdc");
            }

            public IActionResult Index(string data)
            {
                // Xử lý yêu cầu GET theo quy tắc mặc định
                return View("sdcsdc");
            }

            // Không có attribute HTTP nào ở đây, nên mặc định sẽ xử lý yêu cầu GET
            public IActionResult Edit(int id)
            {
                // Xử lý yêu cầu GET theo quy tắc mặc định
                return View("scsdc");
            }
        



        //[HttpPost("Login")]
        //public async Task<IActionResult> Login(LoginUser user)
        //{
        //    if(!ModelState.IsValid)
        //    {
        //        return BadRequest();
        //    }
        //    var result = await _authService.Login(user);
        //    if(result == true)
        //    {
        //        var tokenString = _authService.GenerateTokenString(user);
        //        return Ok(tokenString);
        //    }  
        //    return BadRequest();
        //}

        [AllowAnonymous]
        [HttpPost("Login")]
        public async Task<IActionResult> Login(LoginUser user)
        {
            if(!ModelState.IsValid)
            {
                return BadRequest();
            }
            var result = await _authService.Login(user);
            if(result != null)
            {
                var tokenString = _authService.GenerateTokenString(user);
                return Ok(tokenString);
            }  
            return NotFound("User Not Found");
        }
    
}
