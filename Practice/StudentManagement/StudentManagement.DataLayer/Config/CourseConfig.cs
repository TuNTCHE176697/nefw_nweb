﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StudentManagement.DataLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.DataLayer.Config
{
    public class CourseConfig : IEntityTypeConfiguration<Course>
    {
        public void Configure(EntityTypeBuilder<Course> builder)
        {
            builder.ToTable("Course");
            builder.HasKey(x => x.CourseID);
            builder.Property(x => x.CourseID).ValueGeneratedOnAdd();
            builder.Property(x => x.CourseName).IsUnicode(true);
            builder.HasData(
                new Course { CourseID = Guid.Parse("fe1cbfe0-93cf-4f0a-a6f2-7e54b371f38d"), CourseName = "Java" },
                new Course { CourseID = Guid.Parse("a7e8771a-5a5d-44f1-92f2-33df854f7639"), CourseName = "C#" },
                new Course { CourseID = Guid.Parse("b5e22b01-6b4b-4eb3-bf56-8f3a68f98a9d"), CourseName = "C" }
            );
        }
    }
}
