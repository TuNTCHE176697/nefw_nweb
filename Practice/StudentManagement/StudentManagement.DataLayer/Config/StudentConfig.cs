﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StudentManagement.DataLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.DataLayer.Config
{
    public class StudentConfig : IEntityTypeConfiguration<Student>
    {
        public void Configure(EntityTypeBuilder<Student> builder)
        {
            builder.ToTable("Student");
            builder.HasKey(x => x.StudentID);
            builder.Property(x => x.StudentID).ValueGeneratedOnAdd();
            builder.Property(x => x.StudentName).IsUnicode(true);
            builder.HasOne(x => x.StudentAdress).WithOne(x => x.Student).HasForeignKey<StudentAdress>(x => x.StudentID);
            builder.HasMany(x => x.Books).WithOne(x => x.Student);

            builder.HasData(
            new Student
            {
                StudentID = Guid.Parse("1c85a7f1-b63d-4311-943a-82e976d3e3bb"),
                StudentName = "Cẩm Tú" 
            },
            new Student
            {
                StudentID = Guid.Parse("f90b719b-4881-4e6f-9bf3-73a345597488"),
                StudentName = "Thanh Hòa"
                
            }
        );
        }
    }
}
