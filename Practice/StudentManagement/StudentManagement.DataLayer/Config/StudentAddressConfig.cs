﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StudentManagement.DataLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.DataLayer.Config
{
    public class StudentAddressConfig :IEntityTypeConfiguration<StudentAdress>
    {
        
        public void Configure(EntityTypeBuilder<StudentAdress> builder)
        {
            builder.ToTable("StudentAddress");
            builder.HasKey(x => x.StudentAdressID);
            builder.Property(x => x.StudentAdressID).ValueGeneratedOnAdd();
            builder.Property(x => x.Address1).IsUnicode(true);
            builder.Property(x => x.Address2).IsUnicode(true);
            builder.HasData(new StudentAdress { StudentAdressID = Guid.NewGuid(), Address1 = "Nghệ An", Address2 = "Thành Phố Vinh", StudentID = Guid.Parse("1c85a7f1-b63d-4311-943a-82e976d3e3bb") },
                            new StudentAdress { StudentAdressID = Guid.NewGuid(), Address1 = "Hải Phòng", Address2 = "An Dương", StudentID = Guid.Parse("f90b719b-4881-4e6f-9bf3-73a345597488") }
            );
        }
    }
}
