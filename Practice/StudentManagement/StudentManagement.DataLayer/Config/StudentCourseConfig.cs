﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StudentManagement.DataLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.DataLayer.Config
{
    public class StudentCourseConfig : IEntityTypeConfiguration<StudentCourse> 
    {
        public void Configure (EntityTypeBuilder<StudentCourse> builder)
        {
            builder.ToTable("StudentCourse");
            builder.HasKey(x => new {x.StudentID, x.CourseID});
            builder.HasOne(x => x.Student).WithMany(x => x.StudentCourse);
            builder.HasOne(x => x.Course).WithMany(x => x.StudentCourse);

            builder.HasData(
                new StudentCourse { StudentID = Guid.Parse("1c85a7f1-b63d-4311-943a-82e976d3e3bb"), CourseID = Guid.Parse("fe1cbfe0-93cf-4f0a-a6f2-7e54b371f38d") },
                new StudentCourse { StudentID = Guid.Parse("1c85a7f1-b63d-4311-943a-82e976d3e3bb"), CourseID = Guid.Parse("a7e8771a-5a5d-44f1-92f2-33df854f7639") },
                new StudentCourse { StudentID = Guid.Parse("f90b719b-4881-4e6f-9bf3-73a345597488"), CourseID = Guid.Parse("fe1cbfe0-93cf-4f0a-a6f2-7e54b371f38d") },
                new StudentCourse { StudentID = Guid.Parse("f90b719b-4881-4e6f-9bf3-73a345597488"), CourseID = Guid.Parse("b5e22b01-6b4b-4eb3-bf56-8f3a68f98a9d") }
                );
        }
    }
}
