﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using StudentManagement.DataLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Reflection.Metadata.BlobBuilder;

namespace StudentManagement.DataLayer.Config
{
    public class BookConfig : IEntityTypeConfiguration<Book>
    {
        public void Configure(EntityTypeBuilder<Book> builder)
        {
            builder.ToTable("Book");
            builder.HasKey(x => x.BookID);
            builder.Property(x => x.BookID).ValueGeneratedOnAdd();
            builder.Property(x => x.BookName).IsUnicode(true);
            builder.HasOne(x => x.Student).WithMany(x => x.Books);
            builder.HasData(
            new Book { BookID = Guid.NewGuid(), BookName = "Book1", StudentID = Guid.Parse("1c85a7f1-b63d-4311-943a-82e976d3e3bb") },
            new Book { BookID = Guid.NewGuid(), BookName = "Book2", StudentID = Guid.Parse("1c85a7f1-b63d-4311-943a-82e976d3e3bb") },
            new Book { BookID = Guid.NewGuid(), BookName = "Book3", StudentID = Guid.Parse("f90b719b-4881-4e6f-9bf3-73a345597488") }, 
            new Book { BookID = Guid.NewGuid(), BookName = "Book4", StudentID = Guid.Parse("f90b719b-4881-4e6f-9bf3-73a345597488") }
            );
        }
    }
}
