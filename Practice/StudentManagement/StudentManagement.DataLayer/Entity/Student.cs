﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.DataLayer.Entity
{
    public class Student 
    {
        public Guid? StudentID { get; set; }
        public string StudentName { get; set; }

        public StudentAdress StudentAdress { get; set; }
        public ICollection<Book> Books { get; set; }
        public ICollection<StudentCourse>? StudentCourse { get; set; }    
    }
}
