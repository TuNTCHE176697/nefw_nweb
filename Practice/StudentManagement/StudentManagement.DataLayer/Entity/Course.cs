﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.DataLayer.Entity
{
    public class Course
    {
        public Guid CourseID { get; set; }
        public string CourseName { get; set; }
        public ICollection<StudentCourse> StudentCourse { get; set; }

    }
}
