﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.DataLayer.Entity.StudentDTO
{
    public class StudentAddressDTO
    {
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public Guid StudentID { get; set; }
    }
}
