﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.DataLayer.Entity.StudentDTO
{
    public class StudentDTO
    {
        public Guid StudentID { get; set; }
        public string StudentName { get; set; }
        public StudentAddressDTO StudentAddress { get; set; }
        public ICollection<BookDTO> Books { get; set; }
    }
}