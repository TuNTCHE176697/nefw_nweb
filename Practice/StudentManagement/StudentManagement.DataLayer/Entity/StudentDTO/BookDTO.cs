﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.DataLayer.Entity.StudentDTO
{
    public class BookDTO
    {
        public Guid BookID { get; set; }
        public string BookName { get; set; }
        public Guid StudentID { get; set; }
    }
}
