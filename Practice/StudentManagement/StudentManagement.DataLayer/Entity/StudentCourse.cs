﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StudentManagement.DataLayer.Entity
{
    public class StudentCourse
    {
        public Guid StudentID { get; set; }
        public Guid CourseID { get; set; }
        public Student Student { get; set; }
        public Course Course { get; set;}
    }
}
