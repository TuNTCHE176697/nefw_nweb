﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace StudentManagement.DataLayer.Migrations
{
    /// <inheritdoc />
    public partial class haha : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "BookID",
                keyValue: new Guid("157f7a12-2505-4f41-bf58-85dbfa2ff1b5"));

            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "BookID",
                keyValue: new Guid("547655be-74da-469e-80d3-55129569e91f"));

            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "BookID",
                keyValue: new Guid("8427d426-17a9-45fa-93f9-7d12bf9ff6f1"));

            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "BookID",
                keyValue: new Guid("e843559b-43a3-4e78-bce5-102c72e2d49f"));

            migrationBuilder.DeleteData(
                table: "StudentAddress",
                keyColumn: "StudentAdressID",
                keyValue: new Guid("a5c18ef5-1a9e-4bdd-a54f-ba98c33ddb66"));

            migrationBuilder.DeleteData(
                table: "StudentAddress",
                keyColumn: "StudentAdressID",
                keyValue: new Guid("b64368b6-a1d8-4145-85e6-46a88dc3cb39"));

            migrationBuilder.AddColumn<int>(
                name: "AccessFailedCount",
                table: "Student",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<string>(
                name: "ConcurrencyStamp",
                table: "Student",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Email",
                table: "Student",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "EmailConfirmed",
                table: "Student",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "Id",
                table: "Student",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "LockoutEnabled",
                table: "Student",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<DateTimeOffset>(
                name: "LockoutEnd",
                table: "Student",
                type: "datetimeoffset",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NormalizedEmail",
                table: "Student",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "NormalizedUserName",
                table: "Student",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PasswordHash",
                table: "Student",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PhoneNumber",
                table: "Student",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "PhoneNumberConfirmed",
                table: "Student",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "SecurityStamp",
                table: "Student",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "TwoFactorEnabled",
                table: "Student",
                type: "bit",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "UserName",
                table: "Student",
                type: "nvarchar(max)",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "AspNetUserLogins",
                columns: table => new
                {
                    LoginProvider = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ProviderKey = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ProviderDisplayName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    UserId = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserRoles",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    RoleId = table.Column<string>(type: "nvarchar(450)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AspNetUserRoles", x => new { x.UserId, x.RoleId });
                });

            migrationBuilder.CreateTable(
                name: "AspNetUserTokens",
                columns: table => new
                {
                    UserId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LoginProvider = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Value = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                });

            migrationBuilder.CreateTable(
                name: "RoleClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    RoleId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RoleClaims", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Roles",
                columns: table => new
                {
                    Id = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    NormalizedName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ConcurrencyStamp = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Roles", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "UserClaims",
                columns: table => new
                {
                    Id = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimType = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ClaimValue = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserClaims", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Book",
                columns: new[] { "BookID", "BookName", "StudentID" },
                values: new object[,]
                {
                    { new Guid("292c1a24-0beb-4614-bf9f-052e52ec3ecf"), "Book4", new Guid("f90b719b-4881-4e6f-9bf3-73a345597488") },
                    { new Guid("297a840f-b3f0-4191-a8e2-437620fe912a"), "Book1", new Guid("1c85a7f1-b63d-4311-943a-82e976d3e3bb") },
                    { new Guid("811e1bba-9741-4749-8e10-5e404a240beb"), "Book3", new Guid("f90b719b-4881-4e6f-9bf3-73a345597488") },
                    { new Guid("accb754d-3e17-4fc0-845c-a5017d2a143a"), "Book2", new Guid("1c85a7f1-b63d-4311-943a-82e976d3e3bb") }
                });

            migrationBuilder.UpdateData(
                table: "Student",
                keyColumn: "StudentID",
                keyValue: new Guid("1c85a7f1-b63d-4311-943a-82e976d3e3bb"),
                columns: new[] { "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "Id", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { 0, "4dea4c53-f816-4254-bf0c-1f4bb8ed089c", null, false, "f81afb3b-c380-4151-810e-79fa781b3f3e", false, null, null, null, null, null, false, "8df75f52-2d12-4b93-8aa4-0a11761a84a9", false, null });

            migrationBuilder.UpdateData(
                table: "Student",
                keyColumn: "StudentID",
                keyValue: new Guid("f90b719b-4881-4e6f-9bf3-73a345597488"),
                columns: new[] { "AccessFailedCount", "ConcurrencyStamp", "Email", "EmailConfirmed", "Id", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "TwoFactorEnabled", "UserName" },
                values: new object[] { 0, "5d136ac7-2b12-43ce-9d8e-d8317c3bcf38", null, false, "19a299a6-3374-4f75-ba91-0d642f48aad7", false, null, null, null, null, null, false, "d300e804-1188-4353-b50d-76eae52c79c9", false, null });

            migrationBuilder.InsertData(
                table: "StudentAddress",
                columns: new[] { "StudentAdressID", "Address1", "Address2", "StudentID" },
                values: new object[,]
                {
                    { new Guid("013068bc-7972-4417-9d42-cdbd18c03f7d"), "Nghệ An", "Thành Phố Vinh", new Guid("1c85a7f1-b63d-4311-943a-82e976d3e3bb") },
                    { new Guid("678c403f-3382-4893-90af-c44326f04c86"), "Hải Phòng", "An Dương", new Guid("f90b719b-4881-4e6f-9bf3-73a345597488") }
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AspNetUserLogins");

            migrationBuilder.DropTable(
                name: "AspNetUserRoles");

            migrationBuilder.DropTable(
                name: "AspNetUserTokens");

            migrationBuilder.DropTable(
                name: "RoleClaims");

            migrationBuilder.DropTable(
                name: "Roles");

            migrationBuilder.DropTable(
                name: "UserClaims");

            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "BookID",
                keyValue: new Guid("292c1a24-0beb-4614-bf9f-052e52ec3ecf"));

            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "BookID",
                keyValue: new Guid("297a840f-b3f0-4191-a8e2-437620fe912a"));

            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "BookID",
                keyValue: new Guid("811e1bba-9741-4749-8e10-5e404a240beb"));

            migrationBuilder.DeleteData(
                table: "Book",
                keyColumn: "BookID",
                keyValue: new Guid("accb754d-3e17-4fc0-845c-a5017d2a143a"));

            migrationBuilder.DeleteData(
                table: "StudentAddress",
                keyColumn: "StudentAdressID",
                keyValue: new Guid("013068bc-7972-4417-9d42-cdbd18c03f7d"));

            migrationBuilder.DeleteData(
                table: "StudentAddress",
                keyColumn: "StudentAdressID",
                keyValue: new Guid("678c403f-3382-4893-90af-c44326f04c86"));

            migrationBuilder.DropColumn(
                name: "AccessFailedCount",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "ConcurrencyStamp",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "Email",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "EmailConfirmed",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "LockoutEnabled",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "LockoutEnd",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "NormalizedEmail",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "NormalizedUserName",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "PasswordHash",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "PhoneNumber",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "PhoneNumberConfirmed",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "SecurityStamp",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "TwoFactorEnabled",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "UserName",
                table: "Student");

            migrationBuilder.InsertData(
                table: "Book",
                columns: new[] { "BookID", "BookName", "StudentID" },
                values: new object[,]
                {
                    { new Guid("157f7a12-2505-4f41-bf58-85dbfa2ff1b5"), "Book3", new Guid("f90b719b-4881-4e6f-9bf3-73a345597488") },
                    { new Guid("547655be-74da-469e-80d3-55129569e91f"), "Book2", new Guid("1c85a7f1-b63d-4311-943a-82e976d3e3bb") },
                    { new Guid("8427d426-17a9-45fa-93f9-7d12bf9ff6f1"), "Book4", new Guid("f90b719b-4881-4e6f-9bf3-73a345597488") },
                    { new Guid("e843559b-43a3-4e78-bce5-102c72e2d49f"), "Book1", new Guid("1c85a7f1-b63d-4311-943a-82e976d3e3bb") }
                });

            migrationBuilder.InsertData(
                table: "StudentAddress",
                columns: new[] { "StudentAdressID", "Address1", "Address2", "StudentID" },
                values: new object[,]
                {
                    { new Guid("a5c18ef5-1a9e-4bdd-a54f-ba98c33ddb66"), "Nghệ An", "Thành Phố Vinh", new Guid("1c85a7f1-b63d-4311-943a-82e976d3e3bb") },
                    { new Guid("b64368b6-a1d8-4145-85e6-46a88dc3cb39"), "Hải Phòng", "An Dương", new Guid("f90b719b-4881-4e6f-9bf3-73a345597488") }
                });
        }
    }
}
