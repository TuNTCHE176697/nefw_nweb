﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace StudentManagement.DataLayer.Migrations
{
    /// <inheritdoc />
    public partial class data : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Course",
                columns: table => new
                {
                    CourseID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CourseName = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Course", x => x.CourseID);
                });

            migrationBuilder.CreateTable(
                name: "Student",
                columns: table => new
                {
                    StudentID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    StudentName = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Student", x => x.StudentID);
                });

            migrationBuilder.CreateTable(
                name: "Book",
                columns: table => new
                {
                    BookID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    BookName = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    StudentID = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Book", x => x.BookID);
                    table.ForeignKey(
                        name: "FK_Book_Student_StudentID",
                        column: x => x.StudentID,
                        principalTable: "Student",
                        principalColumn: "StudentID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StudentAddress",
                columns: table => new
                {
                    StudentAdressID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Address1 = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Address2 = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    StudentID = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentAddress", x => x.StudentAdressID);
                    table.ForeignKey(
                        name: "FK_StudentAddress_Student_StudentID",
                        column: x => x.StudentID,
                        principalTable: "Student",
                        principalColumn: "StudentID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "StudentCourse",
                columns: table => new
                {
                    StudentID = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    CourseID = table.Column<Guid>(type: "uniqueidentifier", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_StudentCourse", x => new { x.StudentID, x.CourseID });
                    table.ForeignKey(
                        name: "FK_StudentCourse_Course_CourseID",
                        column: x => x.CourseID,
                        principalTable: "Course",
                        principalColumn: "CourseID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_StudentCourse_Student_StudentID",
                        column: x => x.StudentID,
                        principalTable: "Student",
                        principalColumn: "StudentID",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Course",
                columns: new[] { "CourseID", "CourseName" },
                values: new object[,]
                {
                    { new Guid("a7e8771a-5a5d-44f1-92f2-33df854f7639"), "C#" },
                    { new Guid("b5e22b01-6b4b-4eb3-bf56-8f3a68f98a9d"), "C" },
                    { new Guid("fe1cbfe0-93cf-4f0a-a6f2-7e54b371f38d"), "Java" }
                });

            migrationBuilder.InsertData(
                table: "Student",
                columns: new[] { "StudentID", "StudentName" },
                values: new object[,]
                {
                    { new Guid("1c85a7f1-b63d-4311-943a-82e976d3e3bb"), "Cẩm Tú" },
                    { new Guid("f90b719b-4881-4e6f-9bf3-73a345597488"), "Thanh Hòa" }
                });

            migrationBuilder.InsertData(
                table: "Book",
                columns: new[] { "BookID", "BookName", "StudentID" },
                values: new object[,]
                {
                    { new Guid("157f7a12-2505-4f41-bf58-85dbfa2ff1b5"), "Book3", new Guid("f90b719b-4881-4e6f-9bf3-73a345597488") },
                    { new Guid("547655be-74da-469e-80d3-55129569e91f"), "Book2", new Guid("1c85a7f1-b63d-4311-943a-82e976d3e3bb") },
                    { new Guid("8427d426-17a9-45fa-93f9-7d12bf9ff6f1"), "Book4", new Guid("f90b719b-4881-4e6f-9bf3-73a345597488") },
                    { new Guid("e843559b-43a3-4e78-bce5-102c72e2d49f"), "Book1", new Guid("1c85a7f1-b63d-4311-943a-82e976d3e3bb") }
                });

            migrationBuilder.InsertData(
                table: "StudentAddress",
                columns: new[] { "StudentAdressID", "Address1", "Address2", "StudentID" },
                values: new object[,]
                {
                    { new Guid("a5c18ef5-1a9e-4bdd-a54f-ba98c33ddb66"), "Nghệ An", "Thành Phố Vinh", new Guid("1c85a7f1-b63d-4311-943a-82e976d3e3bb") },
                    { new Guid("b64368b6-a1d8-4145-85e6-46a88dc3cb39"), "Hải Phòng", "An Dương", new Guid("f90b719b-4881-4e6f-9bf3-73a345597488") }
                });

            migrationBuilder.InsertData(
                table: "StudentCourse",
                columns: new[] { "CourseID", "StudentID" },
                values: new object[,]
                {
                    { new Guid("a7e8771a-5a5d-44f1-92f2-33df854f7639"), new Guid("1c85a7f1-b63d-4311-943a-82e976d3e3bb") },
                    { new Guid("fe1cbfe0-93cf-4f0a-a6f2-7e54b371f38d"), new Guid("1c85a7f1-b63d-4311-943a-82e976d3e3bb") },
                    { new Guid("b5e22b01-6b4b-4eb3-bf56-8f3a68f98a9d"), new Guid("f90b719b-4881-4e6f-9bf3-73a345597488") },
                    { new Guid("fe1cbfe0-93cf-4f0a-a6f2-7e54b371f38d"), new Guid("f90b719b-4881-4e6f-9bf3-73a345597488") }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Book_StudentID",
                table: "Book",
                column: "StudentID");

            migrationBuilder.CreateIndex(
                name: "IX_StudentAddress_StudentID",
                table: "StudentAddress",
                column: "StudentID",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_StudentCourse_CourseID",
                table: "StudentCourse",
                column: "CourseID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Book");

            migrationBuilder.DropTable(
                name: "StudentAddress");

            migrationBuilder.DropTable(
                name: "StudentCourse");

            migrationBuilder.DropTable(
                name: "Course");

            migrationBuilder.DropTable(
                name: "Student");
        }
    }
}
