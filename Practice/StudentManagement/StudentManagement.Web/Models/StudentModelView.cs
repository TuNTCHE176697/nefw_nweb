﻿namespace StudentManagement.Web.Models
{
    public class StudentModelView
    {
        public Guid StudentID { get; set; }
        public string StudentName { get; set; }

        //public StudentAdress StudentAdress { get; set; }
        //public ICollection<Book> Books { get; set; }
        //public ICollection<StudentCourse>? StudentCourse { get; set; }
    }
}
