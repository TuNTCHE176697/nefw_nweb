﻿namespace StudentManagement.Web.Models
{
    public class StudentAddressModelView
    {
        public Guid StudentAdressID { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public Guid StudentID { get; set; }
        public StudentModelView Student { get; set; }
    }
}
