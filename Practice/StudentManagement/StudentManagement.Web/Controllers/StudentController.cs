﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using StudentManagement.Web.Models;

namespace StudentManagement.Web.Controllers
{
    public class StudentController : Controller
    {
        Uri baseAddress = new Uri("http://localhost:5062/api"); 
        private readonly HttpClient _httpClient;

        public StudentController()
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = baseAddress;
        }
        public IActionResult Index()
        {
            List<StudentModelView> students = new List<StudentModelView>();
            HttpResponseMessage response = _httpClient.GetAsync(baseAddress+"/Student/GetStudents").Result;
            if(response.IsSuccessStatusCode)
            {
                string data = response.Content.ReadAsStringAsync().Result;
                students = JsonConvert.DeserializeObject<List<StudentModelView>> (data);
            }
            return View(students);
        }
    }
}
