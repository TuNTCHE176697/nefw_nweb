﻿using FlowerShop.DataLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlowerShop.DataLayer
{
    public static class Until
    {
        public static List<Product> FilterProducts(IEnumerable<Product> products, decimal? minPrice, decimal? maxPrice)
        {
            if (minPrice == null)
            {
                minPrice = 0;
            }
            if (maxPrice == null)
            {
                maxPrice = Int32.MaxValue;
            }
            List<Product> filteredProducts = new List<Product>();
            foreach (var product in products)
            {
                if (product.FlowerSaledPrice.HasValue && minPrice <= product.FlowerSaledPrice.Value && product.FlowerSaledPrice.Value <= maxPrice)
                {
                    filteredProducts.Add(product);
                }
                else if (!product.FlowerSaledPrice.HasValue && minPrice <= product.FlowerPrice && product.FlowerPrice <= maxPrice)
                {
                    filteredProducts.Add(product);
                }
            }
            return filteredProducts;
        }
    }
}
