﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlowerShop.DataLayer.Entity
{
    public class Category
    {
        public Guid CategoryID { get; set; }
        [DisplayName("Category Name")]
        public string CategoryName { get; set; }
        [DisplayName("Display Order")]
        public int CategoryOrder { get; set; }
        public string? CategoryDescription { get; set; }
    }
}
