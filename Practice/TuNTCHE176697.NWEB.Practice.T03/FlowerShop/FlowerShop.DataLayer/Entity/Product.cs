﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.ComponentModel;
using FlowerShop.DataLayer.Enum;

namespace FlowerShop.DataLayer.Entity
{
    public class Product
    {
        public Guid FlowerID { get; set; }

        [DisplayName("Flower Name")]
        public string FlowerName { get; set; }
        [DisplayName("Flower Description")]
        public string? FlowerDescription { get; set; }

        [DisplayName("Flower Color")]
        public Enum.Color FlowerColor { get; set; }
        [DisplayName("Flower Price")]
        public decimal FlowerPrice { get; set; }
        [DisplayName("Flower Saled Price")]
        public decimal? FlowerSaledPrice { get; set; }
        [DisplayName("Flower Store Date")]
        public DateTime StoreDate { get; set; }
        [DisplayName("Flower Store Inventory")]
        public int StoreInventory {  get; set; }
        [DisplayName("Flower Image")]
        
        public string? UrlImage {  get; set; }
        public Guid CategoryID { get; set; }
        public Category? Category { get; set; }
        
    }
}
