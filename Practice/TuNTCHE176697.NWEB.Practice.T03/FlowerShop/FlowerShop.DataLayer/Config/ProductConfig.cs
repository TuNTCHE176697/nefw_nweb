﻿using FlowerShop.DataLayer.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlowerShop.DataLayer.Config
{
    public class ProductConfig : IEntityTypeConfiguration<Product>
    {
        public void Configure(EntityTypeBuilder<Product> builder)
        {
            builder.ToTable("Flower");
            builder.HasKey(x => x.FlowerID);
            builder.Property(x => x.FlowerID).ValueGeneratedOnAdd();
            builder.Property(x => x.FlowerName).IsUnicode(true).HasMaxLength(255);
            builder.Property(x => x.FlowerDescription);
            builder.Property(x => x.FlowerColor);
            builder.Property(x => x.FlowerPrice).IsRequired();
            builder.Property(x => x.FlowerSaledPrice);
            builder.Property(x => x.StoreDate).IsRequired();
            builder.Property(x => x.StoreInventory).IsRequired();
            builder.Property(x => x.UrlImage);
            builder.HasData(
                new Product {FlowerID=Guid.NewGuid(), FlowerName = "Red Rose", FlowerDescription = "Classic red rose", FlowerColor = Enum.Color.Red, UrlImage = "\\images\\products\\product1.jpg", FlowerPrice = 12.99m, StoreDate = DateTime.Now, StoreInventory = 50, CategoryID = Guid.Parse("91DFAFB0-D5F5-4AB8-A1E4-410F5A2FDE90") },
                new Product { FlowerID = Guid.NewGuid(), FlowerName = "Pink Rose", FlowerDescription = "Beautiful pink rose", FlowerColor = Enum.Color.Pink, UrlImage = "\\images\\products\\product2.jpg", FlowerPrice = 14.99m, StoreDate = DateTime.Now, StoreInventory = 40, CategoryID = Guid.Parse("91DFAFB0-D5F5-4AB8-A1E4-410F5A2FDE90") },
                new Product { FlowerID = Guid.NewGuid(), FlowerName = "White Rose", FlowerDescription = "Elegant white rose", FlowerColor = Enum.Color.Blue, UrlImage = "\\images\\products\\product3.jpg", FlowerPrice = 16.99m, StoreDate = DateTime.Now, StoreInventory = 30, CategoryID = Guid.Parse("8B964AD5-E2F9-4667-B393-E00E1F548C76") },
                new Product { FlowerID = Guid.NewGuid(), FlowerName = "Blue Rose", FlowerDescription = "Elegant blue rose", FlowerColor = Enum.Color.Blue, UrlImage = "\\images\\products\\product4.jpg", FlowerPrice = 17.89m, StoreDate = DateTime.Now, StoreInventory = 31, CategoryID = Guid.Parse("8B964AD5-E2F9-4667-B393-E00E1F548C76") }
            );
        }
    }
}
