﻿using Azure.Core;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlowerShop.DataLayer.Entity;

namespace FlowerShop.DataLayer.Config
{
    public class CategoryConfig : IEntityTypeConfiguration<Category>
    {
        public void Configure(EntityTypeBuilder<Category> builder)
        {
            builder.ToTable("Category");
            builder.HasKey(x => x.CategoryID);
            builder.Property(x => x.CategoryID).ValueGeneratedOnAdd();
            builder.Property(x => x.CategoryName).IsUnicode(true).HasMaxLength(100);
            builder.Property(x => x.CategoryOrder).HasDefaultValue(1);
            builder.Property(x => x.CategoryDescription);

            builder.HasData(
            new Category { CategoryID = Guid.Parse("91DFAFB0-D5F5-4AB8-A1E4-410F5A2FDE90"), CategoryName = "Roses", CategoryOrder = 1, CategoryDescription = "Beautiful roses for all occasions." },
            new Category { CategoryID = Guid.Parse("8B964AD5-E2F9-4667-B393-E00E1F548C76"), CategoryName = "Lilies", CategoryOrder = 2, CategoryDescription = "Elegant lilies for special moments." },
            new Category { CategoryID = Guid.Parse("90180009-8A8B-4750-ABD7-E92CB03FB21A"), CategoryName = "Tulips", CategoryOrder = 2, CategoryDescription = "Colorful tulips to brighten your day." },
            new Category { CategoryID = Guid.Parse("56EB07B3-AAE2-4F8B-BBAD-EF28D7DB81D5"), CategoryName = "Sunflowers", CategoryOrder = 3, CategoryDescription = "Cheerful sunflowers for a sunny disposition." },
            new Category { CategoryID = Guid.Parse("73BA1D11-943D-4405-840A-67E54D9C396D"), CategoryName = "Orchids", CategoryOrder = 4, CategoryDescription = "Exotic orchids for a touch of luxury." }
        );
        }
    }
}
