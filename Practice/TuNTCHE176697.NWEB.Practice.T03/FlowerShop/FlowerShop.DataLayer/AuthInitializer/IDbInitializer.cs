﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlowerShop.DataLayer.AuthInitializer

{
    public interface IDbInitializer
    {
        void Initialize();
    }
}
