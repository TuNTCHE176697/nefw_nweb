﻿using FlowerShop.DataLayer.Context;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlowerShop.DataLayer.AuthInitializer
{
    public class DbInitializer : IDbInitializer
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly FlowerShopDBContext _db;

        public DbInitializer(
            UserManager<IdentityUser> userManager,
            RoleManager<IdentityRole> roleManager,
            FlowerShopDBContext db)
        {
            _roleManager = roleManager;
            _userManager = userManager;
            _db = db;
        }

        public void Initialize()
        {
            if (!_roleManager.RoleExistsAsync("Admin").GetAwaiter().GetResult())
            {
                _roleManager.CreateAsync(new IdentityRole("Admin")).GetAwaiter().GetResult();
                _roleManager.CreateAsync(new IdentityRole("Customer")).GetAwaiter().GetResult();

                _userManager.CreateAsync(new IdentityUser
                {
                    UserName = "admin@gmail.com",
                    Email = "admin@gmail.com",

                }, "Admin123@").GetAwaiter().GetResult();

                IdentityUser admin = _db.IdentityUsers.FirstOrDefault(u => u.Email == "admin@gmail.com");

                _userManager.AddToRoleAsync(admin, "Admin").GetAwaiter().GetResult();


                _userManager.CreateAsync(new IdentityUser
                {
                    UserName = "customer@gmail.com",
                    Email = "customer@gmail.com",


                }, "Customer123@").GetAwaiter().GetResult();

                IdentityUser customer = _db.IdentityUsers.FirstOrDefault(u => u.Email == "customer@gmail.com");

                _userManager.AddToRoleAsync(customer, "Customer").GetAwaiter().GetResult();
            }   
        }
    }
}
