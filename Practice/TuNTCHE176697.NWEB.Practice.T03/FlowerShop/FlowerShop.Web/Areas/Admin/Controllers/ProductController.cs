﻿using FlowerShop.BusinessLayer.IRepository;
using FlowerShop.BusinessLayer.Repository;
using FlowerShop.DataLayer.Entity;
using FlowerShop.DataLayer.Enum;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using NUnit.Framework;
using System.Collections.Generic;
using System.Drawing;

namespace FlowerShop.Web.Areas.Admin.Controllers
{
    [Area("Admin")]
    [Authorize(Roles = "Admin")]
    public class ProductController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public ProductController(IUnitOfWork unitOfWork, IWebHostEnvironment webHostEnvironment)
        {
            _unitOfWork = unitOfWork;
            _webHostEnvironment = webHostEnvironment;
        }
        public IActionResult Index()
        {
            List<Product> objProductList = _unitOfWork.Product.GetAll(includeProperties: "Category").ToList();

            return View(objProductList);
        }

        public IActionResult Upsert(Guid? id)
        {
            IEnumerable<SelectListItem> categoryList = _unitOfWork.Category.GetAll().Select(u => new SelectListItem
            {
                Text = u.CategoryName,
                Value = u.CategoryID.ToString(),
            }
            );
            ViewBag.Categories = categoryList;
            Product product = new Product();

            if (id == null)
            {
                product.FlowerID = Guid.Empty;
                return View(product);
            }
            else
            {
                product = _unitOfWork.Product.Get(u => u.FlowerID == id);
                return View(product);
            }
        }

        [HttpPost]
        public IActionResult Upsert(Product product, IFormFile file)
        {
            if (ModelState.IsValid)
            {
                if (product.FlowerID == Guid.Empty)
                {
                    TempData["success"] = "Product created successfully";
                    _unitOfWork.Product.Add(product);
                }
                else
                {
                    _unitOfWork.Product.Update(product);
                    TempData["success"] = "Product updated successfully";
                }

                _unitOfWork.Save();


                string wwwRootPath = _webHostEnvironment.WebRootPath;
                if (file != null)
                {

                    string fileName = Guid.NewGuid().ToString() + Path.GetExtension(file.FileName);
                    string productPath = @"images\products\product-" + product.FlowerID;
                    string finalPath = Path.Combine(wwwRootPath, productPath);

                    if (!Directory.Exists(finalPath))
                        Directory.CreateDirectory(finalPath);

                    using (var fileStream = new FileStream(Path.Combine(finalPath, fileName), FileMode.Create))
                    {
                        file.CopyTo(fileStream);
                    }

                    product.UrlImage = @"\" + productPath + @"\" + fileName;
                        
                    _unitOfWork.Product.Update(product);
                    _unitOfWork.Save();

                }


                return RedirectToAction("Index");
            }
            else
            {
                IEnumerable<SelectListItem> categoryList = _unitOfWork.Category.GetAll().Select(u => new SelectListItem
                {
                    Text = u.CategoryName,
                    Value = u.CategoryID.ToString(),
                }
            );
                ViewBag.Categories = categoryList;
                return View(product);
            }
        }

        public IActionResult Delete(Guid? id)
        {
            Product obj = _unitOfWork.Product.Get(u => u.FlowerID == id);
            if (obj == null)
            {
                return NotFound();
            }
            _unitOfWork.Product.Remove(obj);
            _unitOfWork.Save();
            TempData["success"] = "Product deleted successfully";
            return RedirectToAction("Index");
        }

        public IActionResult SaleOff()
        {
            IEnumerable<SelectListItem> categories = _unitOfWork.Category.GetAll().Select(u => new SelectListItem
            {
                Text = u.CategoryName,
                Value = u.CategoryID.ToString(),
            }
            );
            ViewBag.Category = categories;
            return View();
        }
        [HttpPost]
        public IActionResult SaleOff(Guid? flowerId, decimal saleOff)
        {
            Product flower = _unitOfWork.Product.Get(x => x.FlowerID == flowerId);
            flower.FlowerSaledPrice = flower.FlowerPrice - (flower.FlowerPrice * saleOff / 100);
            _unitOfWork.Product.Update(flower);
            _unitOfWork.Save();
            TempData["success"] = "SaleOff created successfully";
            return RedirectToAction("Index");
        }

        public JsonResult OnGetGetProducts(Guid? categoryId)
        {
            var products = _unitOfWork.Product.GetFlowerByCategory(categoryId);
            IEnumerable<SelectListItem> result = products.Select(u => new SelectListItem
            {
                Text = u.FlowerName,
                Value = u.FlowerID.ToString(),
            }
            );
            return new JsonResult(result);
        }
    }
}

