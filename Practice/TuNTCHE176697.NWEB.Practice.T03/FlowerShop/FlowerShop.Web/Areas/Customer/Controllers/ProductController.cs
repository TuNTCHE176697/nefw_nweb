﻿using FlowerShop.BusinessLayer.IRepository;
using FlowerShop.DataLayer.Entity;
using FlowerShop.DataLayer;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace FlowerShop.Web.Areas.Customer.Controllers
{
    [Area("Customer")]
    public class ProductController : Controller
    {
        private readonly IUnitOfWork _unitOfWork;
        public ProductController(IUnitOfWork categoryRepository)
        {
            _unitOfWork = categoryRepository;
        }
        public IActionResult Index(Guid? id)
        {
            IEnumerable<Product> flowers = null;
            if (id == null)
            {
                flowers = _unitOfWork.Product.GetAll().ToList();
            }
            else
            {
                flowers = _unitOfWork.Product.GetFlowerByCategory(id);
            }
            
            return View(flowers);
        }

        public IActionResult Search(Guid? categoryId, string? flowerName, decimal? from, decimal? to)
        {
            IEnumerable<Product> flowers = new List<Product>();
            if (categoryId != null)
            {
                //filter by category id
                flowers = _unitOfWork.Product.GetFlowerByCategory(categoryId);
                //filter by name
                flowers = flowers.Where(x => x.FlowerName.ToLower().Contains(flowerName.ToLower())).ToList();
                //filter by price
                flowers = Until.FilterProducts(flowers, from, to);
            }
            IEnumerable<SelectListItem> categoryLists = _unitOfWork.Category.GetAll().Select(u => new SelectListItem
            {
                Text = u.CategoryName,
                Value = u.CategoryID.ToString(),
            }
            );
            ViewBag.categoryList = categoryLists;
            ViewBag.categoryId = categoryId;
            ViewBag.flowerName = flowerName;
            ViewBag.from = from;
            ViewBag.to = to;
            return View(flowers);
        }

        public IActionResult AddToCart()
        {
            TempData["success"] = "The flower is added to cart successfully";
            return RedirectToAction("Index");
        }
    }
}
