﻿using FlowerShop.DataLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlowerShop.BusinessLayer.IRepository
{
    public interface ICategoryRepository : IGenericRepository<Category>
    {
        void Update(Category obj);
    }
}
