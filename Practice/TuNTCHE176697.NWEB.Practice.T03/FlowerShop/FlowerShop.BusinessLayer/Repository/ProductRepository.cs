﻿using FlowerShop.BusinessLayer.IRepository;
using FlowerShop.DataLayer.Context;
using FlowerShop.DataLayer.Entity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlowerShop.BusinessLayer.Repository
{
    public class ProductRepository : GenericRepository<Product>, IProductRepository
    {
        private FlowerShopDBContext _db;
        public ProductRepository(FlowerShopDBContext db) : base(db)
        {
            _db = db;
        }



        public void Update(Product obj)
        {
            _db.Products.Update(obj);
        }
        public IEnumerable<Product> GetFlowerByCategory(Guid? id)
        {
            var products = _db.Products
            .Where(p => p.CategoryID == id)
            .ToList();

            return products;
        }

    }
}
