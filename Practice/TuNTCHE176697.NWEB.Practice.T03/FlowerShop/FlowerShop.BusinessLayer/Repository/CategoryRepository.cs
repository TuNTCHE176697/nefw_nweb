﻿using FlowerShop.BusinessLayer.IRepository;
using FlowerShop.DataLayer.Context;
using FlowerShop.DataLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlowerShop.BusinessLayer.Repository
{
    public class CategoryRepository : GenericRepository<Category>, ICategoryRepository
    {
        private FlowerShopDBContext _db;
        public CategoryRepository(FlowerShopDBContext db) : base(db)
        {
            _db = db;
        }

        public void Update(Category obj)
        {
            _db.Categories.Update(obj);
        }
    }

}
