﻿using FlowerShop.BusinessLayer.IRepository;
using FlowerShop.DataLayer.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FlowerShop.BusinessLayer.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private FlowerShopDBContext _db;
        public ICategoryRepository Category { get; private set; }

        public IProductRepository Product { get; private set; }

        public UnitOfWork(FlowerShopDBContext db)
        {
            _db = db;
       
            Category = new CategoryRepository(_db);
            Product = new ProductRepository(_db);
        }

        public void Save()
        {
            _db.SaveChanges();
        }
    }
}
