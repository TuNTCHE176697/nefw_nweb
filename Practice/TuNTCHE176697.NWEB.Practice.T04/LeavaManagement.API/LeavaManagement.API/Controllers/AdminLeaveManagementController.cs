﻿using LeaveManagement.BusinessLayer.DTO;
using LeaveManagement.BusinessLayer.Irepository;
using LeaveManagement.DataLayer.Entity;
using LeaveManagement.DataLayer.Enum;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Security.Claims;

namespace LeaveManagement.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(Roles = "Admin")]
    public class AdminLeaveManagementController : ControllerBase
    {
        private readonly ILeaveRepository _leaveRepository;

        public AdminLeaveManagementController(ILeaveRepository leaveRepository)
        {
            _leaveRepository = leaveRepository;
        }

        [HttpGet]
        
        public async Task<ActionResult<IEnumerable<object>>> GetAllRequests()
        {
            try
            {
                //var currentUser = GetAccount();

                var requests = await _leaveRepository.GetAllRequests();

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                return Ok(requests);
            }
            catch (Exception ex)
            {
                return StatusCode(500,"Error retrieving data from the database");
            }
        }
        
        [HttpGet]
        
        public async Task<ActionResult<IEnumerable<object>>> GetSumittedRequests()
        {
            try
            {
                //var currentUser = GetAccount();

                var requests = await _leaveRepository.GetSumittedRequests();

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                return Ok(requests);
            }
            catch (Exception ex)
            {
                return StatusCode(500,"Error retrieving data from the database");
            }
        }

        [HttpGet("{requestID:Guid}")]
        public async Task<ActionResult<RequestDTO>> GetRequestByID(Guid requestID)
        {
            try
            {
                var request = await _leaveRepository.GetRequestByID(requestID);

                if (request == null || requestID == null)
                {
                    return NotFound();
                }

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                return Ok(request);
            }
            catch (Exception e)
            {

                return StatusCode(500,"Error retrieving data from the database");
            }
        }
        [HttpPost("{requestID:Guid}")]
        public async Task<ActionResult<Request>> ApproveRequest(Guid requestID)
        {
            try
            {

                if (requestID == null)
                    return NotFound();

                var studentUpdate = await _leaveRepository.GetRequestByID(requestID);

                if (studentUpdate == null)
                    return NotFound($"Request with Id = {requestID} not found");

                var result = await _leaveRepository.ApproveRequest(requestID);
                if (result == null)
                {
                    return BadRequest("Request ID mismatch");

                }
                else return Ok(result);

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Error updating data");
            }
        }
        
        [HttpPost("{requestID:Guid}")]
        public async Task<ActionResult<Request>> RejectRequest(Guid requestID)
        {
            try
            {

                if (requestID == null)
                    return NotFound();

                var studentUpdate = await _leaveRepository.GetRequestByID(requestID);

                if (studentUpdate == null)
                    return NotFound($"Request with Id = {requestID} not found");

                var result = await _leaveRepository.RejectRequest(requestID);
                if (result == null)
                {
                    return BadRequest("Request ID mismatch");

                }
                else return Ok(result);

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Error updating data");
            }
        }


    }
}
