﻿using LeaveManagement.BusinessLayer.DTO;
using LeaveManagement.BusinessLayer.Irepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LeaveManagement.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthRepository _authRepository;

        public AuthController(IAuthRepository authRepository)
        {
            _authRepository = authRepository;
        }


        [AllowAnonymous]
        [HttpPost]
        public async Task<IActionResult> Login(UserLogin user)
        {
            try
            {

                if (!ModelState.IsValid)
                {
                    return BadRequest();
                }

                if (user == null)
                {
                    return BadRequest(ModelState);
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                var result = await _authRepository.Login(user);

                if (result == null)
                {
                    return BadRequest("Account not found!");
                }
                return Ok(result);
            }
            catch(Exception ex)
            {
                return StatusCode(500, "Error from connecting to database");
            }
        }

    }
}
