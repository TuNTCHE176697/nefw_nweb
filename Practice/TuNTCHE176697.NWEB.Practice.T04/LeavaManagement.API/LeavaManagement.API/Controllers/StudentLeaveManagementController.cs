﻿using LeaveManagement.BusinessLayer.DTO;
using LeaveManagement.BusinessLayer.Irepository;
using LeaveManagement.DataLayer.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace LeaveManagement.API.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    [Authorize(Roles = "Student")]
    public class StudentLeaveManagementController : ControllerBase
    {

        private readonly ILeaveRepository _leaveRepository;

        public StudentLeaveManagementController(ILeaveRepository leaveRepository)
        {
            _leaveRepository = leaveRepository;
        }

        [HttpPost]
        public async Task<ActionResult<Request>> CreateRequest(RequestDTO request)
        {
            try
            {

                if (request == null)
                {
                    return BadRequest(ModelState);
                }

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }
    
                var createdRequest = await _leaveRepository.CreateRequest(request);
                if (createdRequest == null)
                {
                    return BadRequest("Not exist StudentID or CourseID or Duplicate registration");
                }
                return Ok(createdRequest);

            }
            catch (Exception ex)
            {
                return StatusCode(500, "Error Creating Request");
            }
        }

        [HttpGet("{requestID:Guid}")]
        public async Task<ActionResult<RequestDTO>> GetRequestByID(Guid requestID)
        {
            try
            {
                var request = await _leaveRepository.GetRequestByID(requestID);

                if (request == null || requestID == null)
                {
                    return NotFound(ModelState);
                }

                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                return Ok(request);
            }
            catch (Exception e)
            {

                return StatusCode(500, "Error retrieving data from the database");
            }
        }

        [HttpGet("{studentID:Guid}")]
        public async Task<ActionResult<IEnumerable<RequestViewModel>>> GetAllRequestByStudentID(Guid studentID )
        {
            try
            {
                if(studentID == null)
                {
                    return NotFound(ModelState);
                }
                //var currentUser = GetAccount();

                var requests = await _leaveRepository.GetAllRequestByStudentID(studentID);

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                return Ok(requests);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Error retrieving data from the database");
            }
        }
        
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Group>>> GetAllClass()
        {
            try
            {

                var requests = await _leaveRepository.GetAllClass();

                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelState);
                }

                return Ok(requests);
            }
            catch (Exception ex)
            {
                return StatusCode(500, "Error retrieving data from the database");
            }
        }

        [HttpPut]
        public async Task<ActionResult<Request>> UpdateRequest(RequestDTO request)
        {
            try
            {
                if (request == null)
                    return BadRequest("Model Request mismatch");
               
                var result = await _leaveRepository.UpdateRequest(request);

                if (result == null)
                {
                    return BadRequest("Request ID mismatch");

                }
                else return Ok(result);
            }
            catch (Exception ex)
            {
                return StatusCode(500,"Error updating data");
            }
        }
        
        [HttpPost("{requestID:Guid}")]
        public async Task<ActionResult<Request>> CancelRequest(Guid requestID)
        {
            try
            {
                if (requestID == null)
                    return NotFound();

                var studentUpdate = await _leaveRepository.GetRequestByID(requestID);

                if (studentUpdate == null)
                    return NotFound($"Request with Id = {requestID} not found");

                var result = await _leaveRepository.CancelRequest(requestID);
                if (result == null)
                {
                    return BadRequest("StudentID mismatch");

                }
                else return Ok(result);

            }
            catch (Exception ex)
            {
                return StatusCode(500,"Error updating data");
            }
        }

    }
}
