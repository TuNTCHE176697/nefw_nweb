﻿using Azure.Core;
using LeaveManagement.BusinessLayer.DTO;
using LeaveManagement.DataLayer.Entity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Text;

namespace LeaveManagement.Web.Controllers
{
    public class LeaveController : Controller
    {
        Uri baseAddress = new Uri("https://localhost:7295/api");
        private readonly HttpClient _httpClient;

        public LeaveController()
        {
            _httpClient = new HttpClient();
            _httpClient.BaseAddress = baseAddress;
        }
        public async Task<IActionResult> Login(UserLogin user)
        {
            StringContent content = new StringContent(JsonConvert.SerializeObject(user), Encoding.UTF8, "application/json");
            var reponse = await _httpClient.PostAsync(baseAddress + "/Auth/Login", content);
            string token = await reponse.Content.ReadAsStringAsync();
            if (token == "Account not found!")
            {
                ViewBag.Message = "Incorrect Email or Password!";
                return View("Index");
            }

            var handler = new JwtSecurityTokenHandler();
            var jsonToken = handler.ReadToken(token) as JwtSecurityToken;
            string userId = jsonToken.Claims.FirstOrDefault(c => c.Type == ClaimTypes.NameIdentifier).Value;
            string userName = jsonToken.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Name)?.Value;
            string userRole = jsonToken.Claims.FirstOrDefault(c => c.Type == ClaimTypes.Role)?.Value;


            HttpContext.Session.SetString("JWToken", token);
            HttpContext.Session.SetString("UserId", userId);
            HttpContext.Session.SetString("UserName", userName);
            HttpContext.Session.SetString("UserRole", userRole);

            ViewBag.Ursername = HttpContext.Session.GetString("UserName");
            return RedirectToAction("Main");

        }

        [HttpGet]
        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public IActionResult AccessDenied()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public IActionResult Main()
        {
            var role = HttpContext.Session.GetString("UserRole");
            var id = HttpContext.Session.GetString("UserId");
            var token = HttpContext.Session.GetString("JWToken");

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            List<RequestViewModel> list = new List<RequestViewModel>();
            if (role == "Admin")
            {
                HttpResponseMessage response = _httpClient.GetAsync(baseAddress + "/AdminLeaveManagement/GetAllRequests").Result;
                if (response.IsSuccessStatusCode)
                {
                    string data = response.Content.ReadAsStringAsync().Result;
                    list = JsonConvert.DeserializeObject<List<RequestViewModel>>(data);
                    return View(list);
                }
                else return RedirectToAction("AccessDenied");
            }
            if (role == "Student")
            {
                HttpResponseMessage response = _httpClient.GetAsync(baseAddress + "/StudentLeaveManagement/GetAllRequestByStudentID/" + Guid.Parse(id)).Result;
                if (response.IsSuccessStatusCode)
                {
                    string data = response.Content.ReadAsStringAsync().Result;
                    list = JsonConvert.DeserializeObject<List<RequestViewModel>>(data);
                    return View(list);
                }
                else return RedirectToAction("AccessDenied");
            }
            return RedirectToAction("AccessDenied");
        }

        [HttpGet]
        public IActionResult Manage()
        {
            var role = HttpContext.Session.GetString("UserRole");
            var token = HttpContext.Session.GetString("JWToken");

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            List<RequestViewModel> list = new List<RequestViewModel>();
            if (role == "Admin")
            {
                HttpResponseMessage response = _httpClient.GetAsync(baseAddress + "/AdminLeaveManagement/GetSumittedRequests").Result;
                if (response.IsSuccessStatusCode)
                {
                    string data = response.Content.ReadAsStringAsync().Result;
                    list = JsonConvert.DeserializeObject<List<RequestViewModel>>(data);
                    return View(list);
                }
                else return RedirectToAction("AccessDenied");
            }           
            return RedirectToAction("AccessDenied");
        }
        [HttpGet]
        public IActionResult Create()
        {
            var token = HttpContext.Session.GetString("JWToken");
            var role = HttpContext.Session.GetString("UserRole");
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            List<Group> list = new List<Group>();
            if (role == "Student")
            {
                HttpResponseMessage response1 = _httpClient.GetAsync(baseAddress + "/StudentLeaveManagement/GetAllClass").Result;
                if (response1.IsSuccessStatusCode)
                {
                    string data1 = response1.Content.ReadAsStringAsync().Result;
                    list = JsonConvert.DeserializeObject<List<Group>>(data1);
                }
                IEnumerable<SelectListItem> classList = list.Select(u => new SelectListItem
                {
                    Text = "Class " + u.GroupName,
                    Value = u.GroupId.ToString(),
                }
                );
                ViewBag.ClassRoom = classList;

                return View();
            }
            else return RedirectToAction("AccessDenied");
        }
        [HttpPost]
        public IActionResult Create(RequestDTO request)
        {
            var token = HttpContext.Session.GetString("JWToken");
            var role = HttpContext.Session.GetString("UserRole");
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            
            try
            {
                if (role == "Student")
                {
                    var id = HttpContext.Session.GetString("UserId");
                    request.AccountID = Guid.Parse(id);
                    request.RequestID = Guid.NewGuid();
                    string data = JsonConvert.SerializeObject(request);
                    StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = _httpClient.PostAsync(baseAddress + "/StudentLeaveManagement/CreateRequest", content).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        TempData["success"] = "Created";
                        return RedirectToAction("Main");
                    }
                    else return RedirectToAction("AccessDenied");
                }
                else return RedirectToAction("AccessDenied");
            }
            catch (Exception ex)
            {     
                return View();
            }
           }

        [HttpGet]
        public IActionResult ViewDetail(Guid id)
        {
            var role = HttpContext.Session.GetString("UserRole");
            var token = HttpContext.Session.GetString("JWToken");

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            RequestDTO requestDetail = new RequestDTO();
            try
            {
                if (role == "Admin")
                {
                    HttpResponseMessage response = _httpClient.GetAsync(baseAddress + "/AdminLeaveManagement/GetRequestByID/" + id).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string data = response.Content.ReadAsStringAsync().Result;
                        requestDetail = JsonConvert.DeserializeObject<RequestDTO>(data);

                    }
                    return View(requestDetail);
                }
                if (role == "Student")
                {
                    HttpResponseMessage response = _httpClient.GetAsync(baseAddress + "/StudentLeaveManagement/GetRequestByID/" + id).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string data = response.Content.ReadAsStringAsync().Result;
                        requestDetail = JsonConvert.DeserializeObject<RequestDTO>(data);

                    }
                    return View(requestDetail);
                }
                return RedirectToAction("AccessDenied");

            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        
        public IActionResult Cancel(Guid requestID)
        {
            var token = HttpContext.Session.GetString("JWToken");
            var role = HttpContext.Session.GetString("UserRole");
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            try
            {
                if (role == "Student")
                {
                    string data = JsonConvert.SerializeObject(requestID);
                    StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = _httpClient.PostAsync(baseAddress + "/StudentLeaveManagement/CancelRequest/"+requestID, content).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Main");
                    }
                    else return RedirectToAction("AccessDenied");
                }
                else return RedirectToAction("AccessDenied");
            }
            catch (Exception ex)
            {
               
                return View();
            }          
        }
        public IActionResult Reject(Guid requestID)
        {
            var token = HttpContext.Session.GetString("JWToken");
            var role = HttpContext.Session.GetString("UserRole");
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            try
            {
                if (role == "Admin")
                {
                    string data = JsonConvert.SerializeObject(requestID);
                    StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = _httpClient.PostAsync(baseAddress + "/AdminLeaveManagement/RejectRequest/" + requestID, content).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Main");
                    }
                    else return RedirectToAction("AccessDenied");
                }
                else return RedirectToAction("AccessDenied");
            }
            catch (Exception ex)
            {
                return View();
            }          
        }
        public IActionResult Approve(Guid requestID)
        {
            var token = HttpContext.Session.GetString("JWToken");
            var role = HttpContext.Session.GetString("UserRole");
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            try
            {
                if (role == "Admin")
                {
                    string data = JsonConvert.SerializeObject(requestID);
                    StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = _httpClient.PostAsync(baseAddress + "/AdminLeaveManagement/ApproveRequest/" + requestID, content).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Main");
                    }
                    else return RedirectToAction("AccessDenied");
                }
                else return RedirectToAction("AccessDenied");
            }
            catch (Exception ex)
            {
                return View();
            }          
        }

        [HttpGet]
        public IActionResult Edit(Guid requestID)
        {
            var role = HttpContext.Session.GetString("UserRole");
            var token = HttpContext.Session.GetString("JWToken");

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            RequestDTO requestDetail = new RequestDTO();
            List<Group> list = new List<Group>();
            HttpResponseMessage response1 = _httpClient.GetAsync(baseAddress + "/StudentLeaveManagement/GetAllClass").Result;
                if (response1.IsSuccessStatusCode)
                {
                    string data1 = response1.Content.ReadAsStringAsync().Result;
                    list = JsonConvert.DeserializeObject<List<Group>>(data1);
                }
                IEnumerable<SelectListItem> classList = list.Select(u => new SelectListItem
                {
                    Text = "Class " + u.GroupName,
                    Value = u.GroupId.ToString(),
                }
                );
                ViewBag.ClassRoom = classList;

            try
            {
                if (role == "Student")
                {
                    HttpResponseMessage response = _httpClient.GetAsync(baseAddress + "/StudentLeaveManagement/GetRequestByID/" + requestID).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        string data = response.Content.ReadAsStringAsync().Result;
                        requestDetail = JsonConvert.DeserializeObject<RequestDTO>(data);
                    }
                    return View(requestDetail);
                }
                return RedirectToAction("AccessDenied");
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
            
        }


        [HttpPost]
        public IActionResult Edit(RequestDTO request)
        {
            var token = HttpContext.Session.GetString("JWToken");
            var role = HttpContext.Session.GetString("UserRole");
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            try
            {
                if (role == "Student")
                {
                    string data = JsonConvert.SerializeObject(request);
                    StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
                    HttpResponseMessage response = _httpClient.PutAsync(baseAddress + "/StudentLeaveManagement/UpdateRequest", content).Result;
                    if (response.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Main");
                    }
                    else return View();
                }
                else return RedirectToAction("AccessDenied");
            }
            catch (Exception ex)
            {
            
                return View();
            }
        }

    }
    }

