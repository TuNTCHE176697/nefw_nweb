﻿using LeaveManagement.BusinessLayer.DTO;
using LeaveManagement.DataLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaveManagement.BusinessLayer.Irepository
{
    public interface IAuthRepository
    {
        string GenerateTokenString(Account loginUser);
        Task<string> Login(UserLogin user);
    }
}
