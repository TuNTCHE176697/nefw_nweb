﻿using LeaveManagement.BusinessLayer.DTO;
using LeaveManagement.DataLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaveManagement.BusinessLayer.Irepository
{
    public interface ILeaveRepository
    {
        Task<IEnumerable<RequestViewModel>> GetAllRequests();
        Task<IEnumerable<RequestViewModel>> GetSumittedRequests();
        Task<RequestDTO> GetRequestByID(Guid id);
        Task<Request> CreateRequest(RequestDTO request);
        Task<IEnumerable<RequestViewModel>>GetAllRequestByStudentID(Guid studentID);
        Task<Request> UpdateRequest(RequestDTO request);
        Task<Request> CancelRequest(Guid requestID);
        Task<Request> ApproveRequest(Guid requestID);
        Task<Request> RejectRequest(Guid requestID);
        Task<IEnumerable<Group>> GetAllClass();
    }
}
