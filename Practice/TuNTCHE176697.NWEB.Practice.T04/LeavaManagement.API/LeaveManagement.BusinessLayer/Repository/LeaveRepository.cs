﻿using LeaveManagement.BusinessLayer.DTO;
using LeaveManagement.BusinessLayer.Irepository;
using LeaveManagement.DataLayer.Context;
using LeaveManagement.DataLayer.Entity;
using LeaveManagement.DataLayer.Enum;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaveManagement.BusinessLayer.Repository
{
    public class LeaveRepository : ILeaveRepository
    {
        protected readonly LeaveManagementDBContext _context;

        public LeaveRepository(LeaveManagementDBContext context)
        {
            _context = context;
        }
        public async Task<IEnumerable<RequestViewModel>> GetAllRequests()
        {
            var requests = await _context.Requests
                           .Join(_context.Groups,
                                 request => request.GroupID,
                                 group => group.GroupId,
                                 (request, group) => new
                                 {
                                     RequestID = request.RequestId,
                                     StudentName = request.StudentName,
                                     ClassName = group.GroupName,
                                     Title = request.Title,
                                     LeaveDate = request.LeaveDate,
                                     LeaveType = request.LeaveType.ToString(),
                                     LeaveStatus = request.Status.ToString()
                                 }
                                 )
                           .OrderByDescending(x => x.LeaveDate)
                           .ToListAsync();
            List<RequestViewModel> models = new List<RequestViewModel>();
            foreach ( var request in requests )
            {
                RequestViewModel requestViewModel = new RequestViewModel();
                requestViewModel.RequestID = request.RequestID;
                requestViewModel.StudentName = request.StudentName;
                requestViewModel.ClassName = request.ClassName;
                requestViewModel.Title = request.Title;
                requestViewModel.LeaveDate = request.LeaveDate;
                requestViewModel.LeaveType = request.LeaveType;
                requestViewModel.LeaveStatus = request.LeaveStatus;
                models.Add( requestViewModel );

            }
            return models;
        }
        
        public async Task<IEnumerable<RequestViewModel>> GetSumittedRequests()
        {
            var requests = await _context.Requests
                           .Where(x => x.Status == Status.Submitted)
                           .Join(_context.Groups,
                                 request => request.GroupID,
                                 group => group.GroupId,
                                 (request, group) => new
                                 {
                                     RequestID = request.RequestId,
                                     StudentName = request.StudentName,
                                     ClassName = group.GroupName,
                                     Title = request.Title,
                                     LeaveDate = request.LeaveDate,
                                     LeaveType = request.LeaveType.ToString(),
                                     LeaveStatus = request.Status.ToString()
                                 }
                                 )
                           .OrderByDescending(x => x.LeaveDate)
                           .ToListAsync();
            List<RequestViewModel> models = new List<RequestViewModel>();
            foreach (var request in requests)
            {
                RequestViewModel requestViewModel = new RequestViewModel();
                requestViewModel.RequestID = request.RequestID;
                requestViewModel.StudentName = request.StudentName;
                requestViewModel.ClassName = request.ClassName;
                requestViewModel.Title = request.Title;
                requestViewModel.LeaveDate = request.LeaveDate;
                requestViewModel.LeaveType = request.LeaveType;
                requestViewModel.LeaveStatus = request.LeaveStatus;
                models.Add(requestViewModel);

            }
            return models;
        }
        
        public async Task<IEnumerable<RequestViewModel>> GetAllRequestByStudentID(Guid studentID)
        {
            var requests = await _context.Requests
                           .Where(x => x.AccountID == studentID)
                           .Join(_context.Groups,
                                 request => request.GroupID,
                                 group => group.GroupId,
                                 (request, group) => new
                                 {
                                     RequestID = request.RequestId,
                                     StudentName = request.StudentName,
                                     ClassName = group.GroupName,
                                     Title = request.Title,
                                     LeaveDate = request.LeaveDate,
                                     LeaveType = request.LeaveType.ToString(),
                                     LeaveStatus = request.Status.ToString()
                                 }
                                 )
                           .OrderByDescending(x => x.LeaveDate)
                           .ToListAsync();
            List<RequestViewModel> models = new List<RequestViewModel>();
            foreach (var request in requests)
            {
                RequestViewModel requestViewModel = new RequestViewModel();
                requestViewModel.RequestID = request.RequestID;
                requestViewModel.StudentName = request.StudentName;
                requestViewModel.ClassName = request.ClassName;
                requestViewModel.Title = request.Title;
                requestViewModel.LeaveDate = request.LeaveDate;
                requestViewModel.LeaveType = request.LeaveType;
                requestViewModel.LeaveStatus = request.LeaveStatus;
                models.Add(requestViewModel);

            }
            return models;
        }

        public async Task<RequestDTO> GetRequestByID(Guid requestID)
        {
            var request = await _context.Requests
                           .Join(_context.Groups,
                                 request => request.GroupID,
                                 group => group.GroupId,
                                 (request, group) => new
                                 {
                                     RequestID = request.RequestId,
                                     StudentName = request.StudentName,
                                     ClassName = group.GroupName,
                                     ClassID = group.GroupId,
                                     Title = request.Title,
                                     LeaveDate = request.LeaveDate,
                                     LeaveType = request.LeaveType,
                                     LeaveStatus = request.Status.ToString(),
                                     Reason = request.Reason,
                                     Type = request.LeaveType.ToString()
                                 }
                                 )
                           .FirstOrDefaultAsync(x => requestID == x.RequestID);
            
            RequestDTO requestViewModel = new RequestDTO();
            requestViewModel.RequestID = request.RequestID;
            requestViewModel.StudentName = request.StudentName;
            requestViewModel.GroupName = request.ClassName;
            requestViewModel.Title = request.Title;
            requestViewModel.LeaveDate = request.LeaveDate;
            requestViewModel.LeaveType = request.LeaveType;
            requestViewModel.Status = request.LeaveStatus;
            requestViewModel.Reason = request.Reason;
            requestViewModel.GroupID = request.ClassID;
            requestViewModel.Type = request.Type;
            return requestViewModel;
        }

        public async Task<Request> CreateRequest(RequestDTO request)
        {
            var requestAfter = new Request
            {
                RequestId = request.RequestID,
                StudentName = request.StudentName,
                Title = request.Title,
                LeaveDate = request.LeaveDate,
                LeaveType = request.LeaveType,
                Reason = request.Reason,
                Status = Status.Submitted,
                AccountID = request.AccountID,
                GroupID = request.GroupID,
            };
            var result = await _context.Requests.AddAsync(requestAfter);
            await _context.SaveChangesAsync();
            return result.Entity;
        }

        public async Task<Request> UpdateRequest(RequestDTO request)
        {
            var requestResult = await _context.Requests.FirstOrDefaultAsync(x => x.RequestId == request.RequestID);

            if (requestResult != null)
            {
                requestResult.StudentName = request.StudentName;
                requestResult.GroupID = request.GroupID;
                requestResult.Title = request.Title;
                requestResult.LeaveDate = request.LeaveDate;
                requestResult.LeaveType = request.LeaveType;
                requestResult.Reason = request.Reason;

                await _context.SaveChangesAsync();
                return requestResult;
            }
            return null;
        }
        
        public async Task<Request> CancelRequest(Guid requestID)
        {
            var requestResult = await _context.Requests.FirstOrDefaultAsync(x => x.RequestId == requestID);

            if (requestResult != null)
            {
                requestResult.Status = Status.Cancelled;
                await _context.SaveChangesAsync();
                return requestResult;
            }
            return null;
        }
        
        public async Task<Request> ApproveRequest(Guid requestID)
        {
            var requestResult = await _context.Requests.FirstOrDefaultAsync(x => x.RequestId == requestID);

            if (requestResult != null)
            {
                requestResult.Status = Status.Approved;
                await _context.SaveChangesAsync();
                return requestResult;
            }
            return null;
        }
        
        public async Task<Request> RejectRequest(Guid requestID)
        {
            var requestResult = await _context.Requests.FirstOrDefaultAsync(x => x.RequestId == requestID);

            if (requestResult != null)
            {
                requestResult.Status = Status.Rejected;
                await _context.SaveChangesAsync();
                return requestResult;
            }
            return null;
        }
        public async Task<IEnumerable<Group>> GetAllClass()
        { 
            var requestResult = await _context.Groups.ToListAsync();

            if (requestResult != null)
            {
               
                return requestResult;
            }
            return null;
        }


    }
}
