﻿
using LeaveManagement.BusinessLayer.DTO;
using LeaveManagement.BusinessLayer.Irepository;
using LeaveManagement.DataLayer.Context;
using LeaveManagement.DataLayer.Entity;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace LeaveManagement.BusinessLayer.Repository
{
    public class AuthRepository : IAuthRepository
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IConfiguration _config;

        protected readonly LeaveManagementDBContext _context;

        public AuthRepository(UserManager<IdentityUser> userManager, IConfiguration config, LeaveManagementDBContext context)
        {
            _userManager = userManager;
            _config = config;
            _context = context;
        }

        public async Task<string> Login(UserLogin loginUser)
        {

            Account identifyUser = await _context.Account.FirstOrDefaultAsync(x => x.AccountUsername == loginUser.Email && x.AccountPassword == loginUser.Password);
            if (identifyUser != null)
            {
                var token = GenerateTokenString(identifyUser);
                return token;
            }
            return null;
        }


        public string GenerateTokenString(Account user)
        {
            var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.NameIdentifier, user.AccountID.ToString()),
                    new Claim(ClaimTypes.Name, user.AccountName),
                    new Claim(ClaimTypes.Role, user.AccountRole.ToString()),
                };

            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config.GetSection("Jwt:Key").Value));

            var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            var securityToken = new JwtSecurityToken(
                claims: claims,
                expires: DateTime.Now.AddMinutes(60),
                issuer: _config.GetSection("Jwt:Issuer").Value,
                audience: _config.GetSection("Jwt:Audience").Value,
                signingCredentials: signingCredentials);

            string tokenString = new JwtSecurityTokenHandler().WriteToken(securityToken);
            return tokenString;
        }
    }
}
