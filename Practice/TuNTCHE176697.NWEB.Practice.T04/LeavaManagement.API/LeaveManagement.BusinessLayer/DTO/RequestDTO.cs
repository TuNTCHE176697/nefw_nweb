﻿using LeaveManagement.DataLayer.Entity;
using LeaveManagement.DataLayer.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaveManagement.BusinessLayer.DTO
{
    public class RequestDTO
    {
        public Guid RequestID { get; set; }
        [Display(Name = "Student Name")]
        [Required(ErrorMessage = "Student Name is required!")]
        [StringLength(100, MinimumLength = 3, ErrorMessage = "Student Name must be more than 3 characters and less than 255 characters!")]
        public string StudentName { get; set; }

        [Display(Name = "Title")]
        [Required(ErrorMessage = "Title is required!")]
        [StringLength(255, MinimumLength = 3, ErrorMessage = "Name must be more than 3 characters and less than 255 characters!")]
        public string Title { get; set; }

        [Display(Name = "Leave Date")]
        [Required(ErrorMessage = "Leave Date is required!")]
        public DateTime LeaveDate { get; set; } = DateTime.Now;

        [Display(Name = "Leave Type")]
        [Required(ErrorMessage = "Leave Type is required!")]
        public LeaveType LeaveType { get; set; }
        public string? Type { get; set; }

        [Display(Name = "Reason for leave")]
        public string Reason { get; set; }
        public Guid AccountID { get; set; }
        public Guid GroupID { get; set; }
        public string? GroupName {  get; set; }
        public string? Status {  get; set; }
    }
}
