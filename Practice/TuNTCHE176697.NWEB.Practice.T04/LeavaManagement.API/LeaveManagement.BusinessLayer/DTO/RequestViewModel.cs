﻿using LeaveManagement.DataLayer.Entity;
using LeaveManagement.DataLayer.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaveManagement.BusinessLayer.DTO
{
    public class RequestViewModel
    {
        public Guid RequestID { get; set; }
        public string StudentName { get; set; }
        public string ClassName { get; set;}
        public string Title { get; set; }                            
        public DateTime LeaveDate { get; set; }
        public string LeaveType { get; set; } 
        public string LeaveStatus { get; set; }
    }
}
