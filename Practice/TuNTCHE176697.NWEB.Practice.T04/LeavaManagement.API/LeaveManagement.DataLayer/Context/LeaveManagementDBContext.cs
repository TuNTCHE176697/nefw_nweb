﻿using LeaveManagement.DataLayer.Config;
using LeaveManagement.DataLayer.Entity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaveManagement.DataLayer.Context
{
    public class LeaveManagementDBContext: IdentityDbContext
    {

        public LeaveManagementDBContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new AccountConfig());
            modelBuilder.ApplyConfiguration(new GroupConfig());
            modelBuilder.ApplyConfiguration(new RequestConfig());
        }

        public DbSet<Account> Account { get; set; }
        public DbSet<Group> Groups { get; set; }
        public DbSet<Request> Requests { get; set; }
    
    }
}
