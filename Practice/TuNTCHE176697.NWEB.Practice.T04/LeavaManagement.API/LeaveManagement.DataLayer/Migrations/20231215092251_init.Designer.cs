﻿// <auto-generated />
using System;
using LeaveManagement.DataLayer.Context;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

#nullable disable

namespace LeaveManagement.DataLayer.Migrations
{
    [DbContext(typeof(LeaveManagementDBContext))]
    [Migration("20231215092251_init")]
    partial class init
    {
        /// <inheritdoc />
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.14")
                .HasAnnotation("Relational:MaxIdentifierLength", 128);

            SqlServerModelBuilderExtensions.UseIdentityColumns(modelBuilder);

            modelBuilder.Entity("LeaveManagement.DataLayer.Entity.Account", b =>
                {
                    b.Property<Guid>("AccountID")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("AccountName")
                        .IsRequired()
                        .IsUnicode(true)
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("AccountPassword")
                        .IsUnicode(false)
                        .HasColumnType("varchar(max)");

                    b.Property<int>("AccountRole")
                        .IsUnicode(false)
                        .HasColumnType("int");

                    b.Property<string>("AccountUsername")
                        .IsUnicode(false)
                        .HasColumnType("varchar(max)");

                    b.Property<Guid?>("GroupId")
                        .HasColumnType("uniqueidentifier");

                    b.HasKey("AccountID");

                    b.HasIndex("GroupId");

                    b.ToTable("Account", (string)null);

                    b.HasData(
                        new
                        {
                            AccountID = new Guid("1c85a7f1-b63d-4311-943a-82e976d3e3bb"),
                            AccountName = "Hoàng Trọng Hiếu",
                            AccountPassword = "Admin123@",
                            AccountRole = 0,
                            AccountUsername = "admin@gmail.com"
                        },
                        new
                        {
                            AccountID = new Guid("f90b719b-4881-4e6f-9bf3-73a345597488"),
                            AccountName = "Nguyễn Thị Cẩm Tú",
                            AccountPassword = "Student123@",
                            AccountRole = 1,
                            AccountUsername = "student@gmail.com",
                            GroupId = new Guid("be18b068-2386-4521-a24b-f58c161dd8d2")
                        },
                        new
                        {
                            AccountID = new Guid("30bd23ce-e80b-4bd8-920d-377080b4d76a"),
                            AccountName = "Hồ Thị Việt Hải",
                            AccountPassword = "Student1234@",
                            AccountRole = 1,
                            AccountUsername = "student1@gmail.com",
                            GroupId = new Guid("6bb7bac1-498a-4d64-9e6b-ed18af15ffdd")
                        });
                });

            modelBuilder.Entity("LeaveManagement.DataLayer.Entity.Group", b =>
                {
                    b.Property<Guid>("GroupId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<string>("GroupName")
                        .IsRequired()
                        .IsUnicode(false)
                        .HasColumnType("varchar(max)");

                    b.HasKey("GroupId");

                    b.ToTable("Group", (string)null);

                    b.HasData(
                        new
                        {
                            GroupId = new Guid("be18b068-2386-4521-a24b-f58c161dd8d2"),
                            GroupName = ".NET 01"
                        },
                        new
                        {
                            GroupId = new Guid("6bb7bac1-498a-4d64-9e6b-ed18af15ffdd"),
                            GroupName = ".NET 02"
                        },
                        new
                        {
                            GroupId = new Guid("f876eb48-b789-41da-8c6a-5b07b192fa2c"),
                            GroupName = "JAVA 01"
                        },
                        new
                        {
                            GroupId = new Guid("c7e66374-b144-491b-9020-abe351d91de8"),
                            GroupName = "JAVA 02"
                        });
                });

            modelBuilder.Entity("LeaveManagement.DataLayer.Entity.Request", b =>
                {
                    b.Property<Guid>("RequestId")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("AccountID")
                        .HasColumnType("uniqueidentifier");

                    b.Property<Guid>("GroupID")
                        .HasColumnType("uniqueidentifier");

                    b.Property<DateTime>("LeaveDate")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("datetime2")
                        .HasDefaultValue(new DateTime(2023, 12, 15, 16, 22, 51, 410, DateTimeKind.Local).AddTicks(5415));

                    b.Property<int>("LeaveType")
                        .HasColumnType("int");

                    b.Property<string>("Reason")
                        .IsRequired()
                        .IsUnicode(true)
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("Status")
                        .HasColumnType("int");

                    b.Property<string>("StudentName")
                        .IsRequired()
                        .HasMaxLength(100)
                        .IsUnicode(true)
                        .HasColumnType("nvarchar(100)");

                    b.Property<string>("Title")
                        .IsRequired()
                        .HasMaxLength(255)
                        .IsUnicode(true)
                        .HasColumnType("nvarchar(255)");

                    b.HasKey("RequestId");

                    b.HasIndex("AccountID");

                    b.HasIndex("GroupID");

                    b.ToTable("Request", (string)null);

                    b.HasData(
                        new
                        {
                            RequestId = new Guid("ec6a1d01-4e74-43cc-ba8f-64939443b40f"),
                            AccountID = new Guid("f90b719b-4881-4e6f-9bf3-73a345597488"),
                            GroupID = new Guid("be18b068-2386-4521-a24b-f58c161dd8d2"),
                            LeaveDate = new DateTime(2023, 12, 15, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            LeaveType = 3,
                            Reason = "Em bị ốm",
                            Status = 0,
                            StudentName = "Nguyễn Thị Cẩm Tú",
                            Title = "Xin nghỉ học ngày 15/12/2023"
                        },
                        new
                        {
                            RequestId = new Guid("2f01b8fb-f18c-4510-938f-b754a16b6693"),
                            AccountID = new Guid("f90b719b-4881-4e6f-9bf3-73a345597488"),
                            GroupID = new Guid("6bb7bac1-498a-4d64-9e6b-ed18af15ffdd"),
                            LeaveDate = new DateTime(2023, 10, 20, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            LeaveType = 2,
                            Reason = "Buổi sáng gia đình có việc bận",
                            Status = 2,
                            StudentName = "Trần Thu Trang",
                            Title = "Xin nghỉ học chiều 20/10"
                        },
                        new
                        {
                            RequestId = new Guid("2d8c3efe-05e1-416c-aad6-95730c5e157d"),
                            AccountID = new Guid("30bd23ce-e80b-4bd8-920d-377080b4d76a"),
                            GroupID = new Guid("f876eb48-b789-41da-8c6a-5b07b192fa2c"),
                            LeaveDate = new DateTime(2023, 11, 6, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            LeaveType = 0,
                            Reason = "Em ngủ quên ",
                            Status = 3,
                            StudentName = "Trương Thị Thanh Hòa",
                            Title = "Xin đến muộn ngày 06/11/2023"
                        },
                        new
                        {
                            RequestId = new Guid("f740b29f-dc59-43d5-b452-6f40eff41fe9"),
                            AccountID = new Guid("30bd23ce-e80b-4bd8-920d-377080b4d76a"),
                            GroupID = new Guid("c7e66374-b144-491b-9020-abe351d91de8"),
                            LeaveDate = new DateTime(2023, 10, 20, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            LeaveType = 0,
                            Reason = "Em bị hư xe",
                            Status = 1,
                            StudentName = "Đỗ Thu Phương",
                            Title = "Xin đến sớm ngày 20/10/2023"
                        },
                        new
                        {
                            RequestId = new Guid("f06134e6-03ce-4063-ac13-fef164d9fada"),
                            AccountID = new Guid("f90b719b-4881-4e6f-9bf3-73a345597488"),
                            GroupID = new Guid("be18b068-2386-4521-a24b-f58c161dd8d2"),
                            LeaveDate = new DateTime(2023, 12, 21, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            LeaveType = 1,
                            Reason = "Em về sớm tổ chức sinh nhật",
                            Status = 1,
                            StudentName = "Bùi Hà Anh",
                            Title = "Xin về sớm ngày 21/12/2023"
                        },
                        new
                        {
                            RequestId = new Guid("5377cd6a-3a30-4a32-9522-5d15b0e19df9"),
                            AccountID = new Guid("f90b719b-4881-4e6f-9bf3-73a345597488"),
                            GroupID = new Guid("be18b068-2386-4521-a24b-f58c161dd8d2"),
                            LeaveDate = new DateTime(2023, 12, 21, 0, 0, 0, 0, DateTimeKind.Unspecified),
                            LeaveType = 3,
                            Reason = "Em phải vào viện",
                            Status = 0,
                            StudentName = "Nguyễn Thị Cẩm Tú",
                            Title = "Xin nghỉ ngày 21/12/2023"
                        });
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRole", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Name")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<string>("NormalizedName")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedName")
                        .IsUnique()
                        .HasDatabaseName("RoleNameIndex")
                        .HasFilter("[NormalizedName] IS NOT NULL");

                    b.ToTable("AspNetRoles", (string)null);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("ClaimType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("RoleId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("Id");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetRoleClaims", (string)null);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUser", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("nvarchar(450)");

                    b.Property<int>("AccessFailedCount")
                        .HasColumnType("int");

                    b.Property<string>("ConcurrencyStamp")
                        .IsConcurrencyToken()
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("Email")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<bool>("EmailConfirmed")
                        .HasColumnType("bit");

                    b.Property<bool>("LockoutEnabled")
                        .HasColumnType("bit");

                    b.Property<DateTimeOffset?>("LockoutEnd")
                        .HasColumnType("datetimeoffset");

                    b.Property<string>("NormalizedEmail")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<string>("NormalizedUserName")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.Property<string>("PasswordHash")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("PhoneNumber")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("PhoneNumberConfirmed")
                        .HasColumnType("bit");

                    b.Property<string>("SecurityStamp")
                        .HasColumnType("nvarchar(max)");

                    b.Property<bool>("TwoFactorEnabled")
                        .HasColumnType("bit");

                    b.Property<string>("UserName")
                        .HasMaxLength(256)
                        .HasColumnType("nvarchar(256)");

                    b.HasKey("Id");

                    b.HasIndex("NormalizedEmail")
                        .HasDatabaseName("EmailIndex");

                    b.HasIndex("NormalizedUserName")
                        .IsUnique()
                        .HasDatabaseName("UserNameIndex")
                        .HasFilter("[NormalizedUserName] IS NOT NULL");

                    b.ToTable("AspNetUsers", (string)null);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int");

                    SqlServerPropertyBuilderExtensions.UseIdentityColumn(b.Property<int>("Id"));

                    b.Property<string>("ClaimType")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ClaimValue")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("Id");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserClaims", (string)null);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.Property<string>("LoginProvider")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ProviderKey")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("ProviderDisplayName")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("UserId")
                        .IsRequired()
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("LoginProvider", "ProviderKey");

                    b.HasIndex("UserId");

                    b.ToTable("AspNetUserLogins", (string)null);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("RoleId")
                        .HasColumnType("nvarchar(450)");

                    b.HasKey("UserId", "RoleId");

                    b.HasIndex("RoleId");

                    b.ToTable("AspNetUserRoles", (string)null);
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.Property<string>("UserId")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("LoginProvider")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(450)");

                    b.Property<string>("Value")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("UserId", "LoginProvider", "Name");

                    b.ToTable("AspNetUserTokens", (string)null);
                });

            modelBuilder.Entity("LeaveManagement.DataLayer.Entity.Account", b =>
                {
                    b.HasOne("LeaveManagement.DataLayer.Entity.Group", "Group")
                        .WithMany("Accounts")
                        .HasForeignKey("GroupId");

                    b.Navigation("Group");
                });

            modelBuilder.Entity("LeaveManagement.DataLayer.Entity.Request", b =>
                {
                    b.HasOne("LeaveManagement.DataLayer.Entity.Account", "Account")
                        .WithMany("Requests")
                        .HasForeignKey("AccountID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("LeaveManagement.DataLayer.Entity.Group", "Group")
                        .WithMany("Requests")
                        .HasForeignKey("GroupID")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.Navigation("Account");

                    b.Navigation("Group");
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityRoleClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserClaim<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityUser", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserLogin<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityUser", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserRole<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityRole", null)
                        .WithMany()
                        .HasForeignKey("RoleId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityUser", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("Microsoft.AspNetCore.Identity.IdentityUserToken<string>", b =>
                {
                    b.HasOne("Microsoft.AspNetCore.Identity.IdentityUser", null)
                        .WithMany()
                        .HasForeignKey("UserId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });

            modelBuilder.Entity("LeaveManagement.DataLayer.Entity.Account", b =>
                {
                    b.Navigation("Requests");
                });

            modelBuilder.Entity("LeaveManagement.DataLayer.Entity.Group", b =>
                {
                    b.Navigation("Accounts");

                    b.Navigation("Requests");
                });
#pragma warning restore 612, 618
        }
    }
}
