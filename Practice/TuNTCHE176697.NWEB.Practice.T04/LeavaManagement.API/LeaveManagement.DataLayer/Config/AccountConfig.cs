﻿using LeaveManagement.DataLayer.Entity;
using LeaveManagement.DataLayer.Enum;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaveManagement.DataLayer.Config
{
    public class AccountConfig : IEntityTypeConfiguration<Account>
    {
        public void Configure(EntityTypeBuilder<Account> builder)
        {
            builder.ToTable("Account");
            builder.HasKey(x => x.AccountID);
            builder.Property(x => x.AccountID).ValueGeneratedOnAdd();
            builder.Property(x => x.AccountName).IsUnicode(true);
            builder.Property(x => x.AccountRole).IsUnicode(false);
            builder.Property(x => x.AccountUsername).IsUnicode(false);
            builder.Property(x => x.AccountPassword).IsUnicode(false);
            builder.HasMany(x => x.Requests).WithOne(x => x.Account);
            builder.HasOne(x => x.Group).WithMany(x => x.Accounts);

            builder.HasData(
                new Account
                {
                    AccountID = Guid.Parse("1c85a7f1-b63d-4311-943a-82e976d3e3bb"),
                    AccountName = "Hoàng Trọng Hiếu",
                    AccountRole = Role.Admin,
                    AccountUsername = "admin@gmail.com",
                    AccountPassword = "Admin123@",

                },
                new Account
                {
                    AccountID = Guid.Parse("f90b719b-4881-4e6f-9bf3-73a345597488"),
                    AccountName = "Nguyễn Thị Cẩm Tú",
                    AccountRole = Role.Student,
                    AccountUsername = "student@gmail.com",
                    AccountPassword = "Student123@",
                    GroupId = Guid.Parse("BE18B068-2386-4521-A24B-F58C161DD8D2")
                },
                
                new Account
                {
                    AccountID = Guid.Parse("30BD23CE-E80B-4BD8-920D-377080B4D76A"),
                    AccountName = "Hồ Thị Việt Hải",
                    AccountRole = Role.Student,
                    AccountUsername = "student1@gmail.com",
                    AccountPassword = "Student1234@",
                    GroupId = Guid.Parse("6BB7BAC1-498A-4D64-9E6B-ED18AF15FFDD")
                }
            );
        }
    }
}
