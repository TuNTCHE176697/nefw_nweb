﻿using Azure.Core;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LeaveManagement.DataLayer.Entity;
using LeaveManagement.DataLayer.Enum;
using Request = LeaveManagement.DataLayer.Entity.Request;
using Microsoft.Identity.Client;
using System.Text.RegularExpressions;

namespace LeaveManagement.DataLayer.Config
{
    public class RequestConfig : IEntityTypeConfiguration<Request>
    {
        public void Configure(EntityTypeBuilder<Request> builder)
        {
            builder.ToTable("Request");
            builder.HasKey(x => x.RequestId);
            builder.Property(x => x.RequestId).ValueGeneratedOnAdd();
            builder.Property(x => x.StudentName).IsUnicode(true).HasMaxLength(100);
            builder.Property(x => x.Title).HasMaxLength(255).IsUnicode(true);
            builder.Property(x => x.LeaveDate).HasDefaultValue(DateTime.Now);
            builder.Property(x => x.LeaveType);
            builder.Property(x => x.Reason).IsUnicode(true);
            builder.Property(x => x.Status);
            builder.HasOne(x => x.Account).WithMany(x => x.Requests);
            builder.HasOne(x => x.Group).WithMany(x => x.Requests);


            builder.HasData(
                new Request()
                {
                    RequestId = Guid.NewGuid(),
                    StudentName = "Nguyễn Thị Cẩm Tú",
                    Title = "Xin nghỉ học ngày 15/12/2023",
                    LeaveDate = new DateTime(2023, 12, 15),
                    LeaveType = LeaveType.LeaveOneDay,
                    Reason = "Em bị ốm",
                    Status = Status.Submitted,
                    AccountID = Guid.Parse("f90b719b-4881-4e6f-9bf3-73a345597488"),
                    GroupID = Guid.Parse("BE18B068-2386-4521-A24B-F58C161DD8D2"),
                },

                new Request()
                {
                    RequestId = Guid.NewGuid(),
                    StudentName = "Trần Thu Trang",
                    Title = "Xin nghỉ học chiều 20/10",
                    LeaveDate = new DateTime(2023, 10, 20),
                    LeaveType = LeaveType.LeaveAHaftOfDay,
                    Reason = "Buổi sáng gia đình có việc bận",
                    Status = Status.Approved,
                    AccountID = Guid.Parse("f90b719b-4881-4e6f-9bf3-73a345597488"),
                    GroupID = Guid.Parse("6BB7BAC1-498A-4D64-9E6B-ED18AF15FFDD"),
                },

                new Request()
                {
                    RequestId = Guid.NewGuid(),
                    StudentName = "Trương Thị Thanh Hòa",
                    Title = "Xin đến muộn ngày 06/11/2023",
                    LeaveDate = new DateTime(2023, 11, 06),
                    LeaveType = LeaveType.LateComing,
                    Reason = "Em ngủ quên ",
                    Status = Status.Rejected,
                    AccountID = Guid.Parse("30BD23CE-E80B-4BD8-920D-377080B4D76A"),
                    GroupID = Guid.Parse("F876EB48-B789-41DA-8C6A-5B07B192FA2C"),
                },

                new Request()
                {
                    RequestId = Guid.NewGuid(),
                    StudentName = "Đỗ Thu Phương",
                    Title = "Xin đến sớm ngày 20/10/2023",
                    LeaveDate = new DateTime(2023, 10, 20),
                    LeaveType = LeaveType.LateComing,
                    Reason = "Em bị hư xe",
                    Status = Status.Cancelled,
                    AccountID = Guid.Parse("30BD23CE-E80B-4BD8-920D-377080B4D76A"),
                    GroupID = Guid.Parse("C7E66374-B144-491B-9020-ABE351D91DE8"),
                },

                new Request()
                {
                    RequestId = Guid.NewGuid(),
                    StudentName = "Bùi Hà Anh",
                    Title = "Xin về sớm ngày 21/12/2023",
                    LeaveDate = new DateTime(2023, 12, 21),
                    LeaveType = LeaveType.EarlyLeave,
                    Reason = "Em về sớm tổ chức sinh nhật",
                    Status = Status.Cancelled,
                    AccountID = Guid.Parse("f90b719b-4881-4e6f-9bf3-73a345597488"),
                    GroupID = Guid.Parse("BE18B068-2386-4521-A24B-F58C161DD8D2"),
                },
                new Request()
                {
                    RequestId = Guid.NewGuid(),
                    StudentName = "Nguyễn Thị Cẩm Tú",
                    Title = "Xin nghỉ ngày 21/12/2023",
                    LeaveDate = new DateTime(2023, 12, 21),
                    LeaveType = LeaveType.LeaveOneDay,
                    Reason = "Em phải vào viện",
                    Status = Enum.Status.Submitted,
                    AccountID = Guid.Parse("f90b719b-4881-4e6f-9bf3-73a345597488"),
                    GroupID = Guid.Parse("BE18B068-2386-4521-A24B-F58C161DD8D2"),
                }
            );
        }
    }
}

