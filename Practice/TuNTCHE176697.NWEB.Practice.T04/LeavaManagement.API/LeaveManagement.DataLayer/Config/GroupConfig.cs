﻿using LeaveManagement.DataLayer.Entity;
using LeaveManagement.DataLayer.Enum;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaveManagement.DataLayer.Config
{
    public class GroupConfig : IEntityTypeConfiguration<Group>
    {
        public void Configure(EntityTypeBuilder<Group> builder)
        {
            builder.ToTable("Group");
            builder.HasKey(x => x.GroupId);
            builder.Property(x => x.GroupId).ValueGeneratedOnAdd();
            builder.Property(x => x.GroupName).IsUnicode(false);          
            builder.HasMany(x => x.Requests).WithOne(x => x.Group);
            builder.HasMany(x => x.Accounts).WithOne(x => x.Group);

            builder.HasData(
                new Group
                {
                    GroupId = Guid.Parse("BE18B068-2386-4521-A24B-F58C161DD8D2"),
                    GroupName = ".NET 01",

                },
                new Group
                {
                    GroupId = Guid.Parse("6BB7BAC1-498A-4D64-9E6B-ED18AF15FFDD"),
                    GroupName = ".NET 02",

                },
                new Group
                {
                    GroupId = Guid.Parse("F876EB48-B789-41DA-8C6A-5B07B192FA2C"),
                    GroupName = "JAVA 01",

                },
                new Group
                {
                    GroupId = Guid.Parse("C7E66374-B144-491B-9020-ABE351D91DE8"),
                    GroupName = "JAVA 02",

                }
             ); 
        }
    }
}
