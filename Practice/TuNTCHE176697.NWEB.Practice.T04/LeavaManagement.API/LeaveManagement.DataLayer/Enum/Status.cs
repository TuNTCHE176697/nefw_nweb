﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaveManagement.DataLayer.Enum
{
    public enum Status
    {
        Submitted,
        Cancelled,
        Approved,
        Rejected
    }
}
