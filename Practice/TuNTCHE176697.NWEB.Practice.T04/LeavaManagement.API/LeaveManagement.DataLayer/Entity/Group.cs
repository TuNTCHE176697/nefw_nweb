﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaveManagement.DataLayer.Entity
{
    public class Group
    {
        public Guid GroupId { get; set; }
        public string GroupName { get; set; }

        public IEnumerable<Account>? Accounts { get; set;}
        public IEnumerable<Request>? Requests { get; set;}
    }
}
