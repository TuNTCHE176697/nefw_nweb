﻿using LeaveManagement.DataLayer.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaveManagement.DataLayer.Entity
{
    public class Request
    {
        public Guid RequestId { get; set; }
        
        public string StudentName { get; set; }
        
        public string Title { get; set; }
        
        public DateTime LeaveDate { get; set; } = DateTime.Now;
        
        public LeaveType LeaveType { get; set; }
        
        public string Reason { get; set; }
        public Status Status { get; set; }
        public Guid AccountID { get; set; }
        public Account Account { get; set; }
        public Guid GroupID { get; set; }
        public Group Group { get; set; }
    }
}
