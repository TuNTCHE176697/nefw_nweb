﻿using LeaveManagement.DataLayer.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LeaveManagement.DataLayer.Entity
{
    public class Account
    {
        public Guid AccountID { get; set; }

        public string AccountName { get; set; }

        public Role  AccountRole { get; set; }
        public string? AccountUsername { get; set; }
        public string? AccountPassword { get; set; }
        public ICollection<Request>? Requests { get; set; }
        public Guid? GroupId { get; set; }
        public Group? Group { get; set; }
    }
}
