﻿using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;

namespace DemoDatabaseFirst.Entity;

public partial class MovieCollectionContext : DbContext
{
    public MovieCollectionContext()
    {
    }

    public MovieCollectionContext(DbContextOptions<MovieCollectionContext> options)
        : base(options)
    {
    }

    public virtual DbSet<ActedIn> ActedIns { get; set; }

    public virtual DbSet<Actor> Actors { get; set; }

    public virtual DbSet<Employee> Employees { get; set; }

    public virtual DbSet<Movie> Movies { get; set; }

    public virtual DbSet<Project> Projects { get; set; }

    public virtual DbSet<ProjectModule> ProjectModules { get; set; }

    public virtual DbSet<WorkDone> WorkDones { get; set; }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
        => optionsBuilder.UseSqlServer("Data Source=DESKTOP-351RKVI\\CAMTU;Initial Catalog=MovieCollection;Integrated Security=True");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<ActedIn>(entity =>
        {
            entity.HasKey(e => new { e.MovieNo, e.ActorNo }).HasName("PK__ActedIn__DEAFF971A1E638BA");

            entity.ToTable("ActedIn");

            entity.Property(e => e.CharacterRole).HasMaxLength(100);
            entity.Property(e => e.Description).HasColumnType("ntext");

            entity.HasOne(d => d.ActorNoNavigation).WithMany(p => p.ActedIns)
                .HasForeignKey(d => d.ActorNo)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__ActedIn__ActorNo__3E52440B");

            entity.HasOne(d => d.MovieNoNavigation).WithMany(p => p.ActedIns)
                .HasForeignKey(d => d.MovieNo)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("FK__ActedIn__MovieNo__3D5E1FD2");
        });

        modelBuilder.Entity<Actor>(entity =>
        {
            entity.HasKey(e => e.ActorNo).HasName("PK__Actor__57B3C3B86A2A4304");

            entity.ToTable("Actor");

            entity.Property(e => e.ActorName).HasMaxLength(50);
            entity.Property(e => e.ActorSalary).HasColumnType("money");
            entity.Property(e => e.Nationality).HasMaxLength(100);
        });

        modelBuilder.Entity<Employee>(entity =>
        {
            entity.HasKey(e => e.EmployeeId).HasName("PK__Employee__7AD04FF1920D8E16");

            entity.ToTable("Employee");

            entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");
            entity.Property(e => e.EmployeeFirstName).HasMaxLength(20);
            entity.Property(e => e.EmployeeHireDate).HasColumnType("date");
            entity.Property(e => e.EmployeeLastName).HasMaxLength(20);
            entity.Property(e => e.SocialSecurityNumber)
                .HasMaxLength(100)
                .IsUnicode(false);
            entity.Property(e => e.SupervisorId).HasColumnName("SupervisorID");

            entity.HasOne(d => d.Supervisor).WithMany(p => p.InverseSupervisor)
                .HasForeignKey(d => d.SupervisorId)
                .HasConstraintName("FK__Employee__Superv__440B1D61");
        });

        modelBuilder.Entity<Movie>(entity =>
        {
            entity.HasKey(e => e.MovieNo).HasName("PK__Movie__4BD4C54A7A67678D");

            entity.ToTable("Movie");

            entity.HasIndex(e => e.ImageLink, "UQ__Movie__CF729F9AAD938D4C").IsUnique();

            entity.Property(e => e.AmountOfMoney).HasColumnType("money");
            entity.Property(e => e.Comment).HasColumnType("ntext");
            entity.Property(e => e.Director).HasMaxLength(50);
            entity.Property(e => e.ImageLink).HasMaxLength(255);
            entity.Property(e => e.MovieName).HasMaxLength(100);
        });

        modelBuilder.Entity<Project>(entity =>
        {
            entity.HasKey(e => e.ProjectId).HasName("PK__Projects__761ABED00D1016C4");

            entity.Property(e => e.ProjectId).HasColumnName("ProjectID");
            entity.Property(e => e.ProjectCommpletedOn).HasColumnType("date");
            entity.Property(e => e.ProjectDescription).HasColumnType("ntext");
            entity.Property(e => e.ProjectDetail).HasColumnType("ntext");
            entity.Property(e => e.ProjectName).HasMaxLength(100);
            entity.Property(e => e.ProjectStartDate).HasColumnType("date");
        });

        modelBuilder.Entity<ProjectModule>(entity =>
        {
            entity.HasKey(e => e.ModuleId).HasName("PK__Project___2B7477872EBB4C80");

            entity.ToTable("Project_Modules");

            entity.Property(e => e.ModuleId).HasColumnName("ModuleID");
            entity.Property(e => e.EmployeeId).HasColumnName("EmployeeID");
            entity.Property(e => e.ProjectId).HasColumnName("ProjectID");
            entity.Property(e => e.ProjectModulesCompletedOn).HasColumnType("date");
            entity.Property(e => e.ProjectModulesDate).HasColumnType("date");
            entity.Property(e => e.ProjectModulesDescription).HasColumnType("ntext");

            entity.HasOne(d => d.Employee).WithMany(p => p.ProjectModules)
                .HasForeignKey(d => d.EmployeeId)
                .HasConstraintName("FK__Project_M__Emplo__47DBAE45");

            entity.HasOne(d => d.Project).WithMany(p => p.ProjectModules)
                .HasForeignKey(d => d.ProjectId)
                .HasConstraintName("FK__Project_M__Proje__46E78A0C");
        });

        modelBuilder.Entity<WorkDone>(entity =>
        {
            entity.HasKey(e => e.WorkDoneId).HasName("PK__Work_Don__F75A14110D478DD9");

            entity.ToTable("Work_Done");

            entity.Property(e => e.WorkDoneId).HasColumnName("WorkDoneID");
            entity.Property(e => e.ModuleId).HasColumnName("ModuleID");
            entity.Property(e => e.WorkDoneDate).HasColumnType("date");
            entity.Property(e => e.WorkDoneDescription).HasColumnType("ntext");

            entity.HasOne(d => d.Module).WithMany(p => p.WorkDones)
                .HasForeignKey(d => d.ModuleId)
                .HasConstraintName("FK__Work_Done__Modul__4AB81AF0");
        });

        OnModelCreatingPartial(modelBuilder);
    }

    partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
}
