﻿using System;
using System.Collections.Generic;

namespace DemoDatabaseFirst.Entity;

public partial class ProjectModule
{
    public int ModuleId { get; set; }

    public int? ProjectId { get; set; }

    public int? EmployeeId { get; set; }

    public DateTime ProjectModulesDate { get; set; }

    public DateTime? ProjectModulesCompletedOn { get; set; }

    public string? ProjectModulesDescription { get; set; }

    public virtual Employee? Employee { get; set; }

    public virtual Project? Project { get; set; }

    public virtual ICollection<WorkDone> WorkDones { get; set; } = new List<WorkDone>();
}
