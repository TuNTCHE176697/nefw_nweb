﻿using System;
using System.Collections.Generic;

namespace DemoDatabaseFirst.Entity;

public partial class Employee
{
    public int EmployeeId { get; set; }

    public string EmployeeLastName { get; set; } = null!;

    public string EmployeeFirstName { get; set; } = null!;

    public DateTime EmployeeHireDate { get; set; }

    public bool EmployeeStatus { get; set; }

    public int? SupervisorId { get; set; }

    public string? SocialSecurityNumber { get; set; }

    public virtual ICollection<Employee> InverseSupervisor { get; set; } = new List<Employee>();

    public virtual ICollection<ProjectModule> ProjectModules { get; set; } = new List<ProjectModule>();

    public virtual Employee? Supervisor { get; set; }
}
