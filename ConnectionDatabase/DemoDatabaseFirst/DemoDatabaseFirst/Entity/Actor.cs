﻿using System;
using System.Collections.Generic;

namespace DemoDatabaseFirst.Entity;

public partial class Actor
{
    public int ActorNo { get; set; }

    public string ActorName { get; set; } = null!;

    public decimal ActorSalary { get; set; }

    public string Nationality { get; set; } = null!;

    public int Age { get; set; }

    public virtual ICollection<ActedIn> ActedIns { get; set; } = new List<ActedIn>();
}
