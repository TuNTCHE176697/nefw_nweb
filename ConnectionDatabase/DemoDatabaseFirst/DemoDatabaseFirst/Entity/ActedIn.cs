﻿using System;
using System.Collections.Generic;

namespace DemoDatabaseFirst.Entity;

public partial class ActedIn
{
    public int MovieNo { get; set; }

    public int ActorNo { get; set; }

    public string CharacterRole { get; set; } = null!;

    public string? Description { get; set; }

    public virtual Actor ActorNoNavigation { get; set; } = null!;

    public virtual Movie MovieNoNavigation { get; set; } = null!;
}
