﻿using System;
using System.Collections.Generic;

namespace DemoDatabaseFirst.Entity;

public partial class WorkDone
{
    public int WorkDoneId { get; set; }

    public int? ModuleId { get; set; }

    public DateTime? WorkDoneDate { get; set; }

    public string? WorkDoneDescription { get; set; }

    public bool? WorkDoneStatus { get; set; }

    public virtual ProjectModule? Module { get; set; }
}
