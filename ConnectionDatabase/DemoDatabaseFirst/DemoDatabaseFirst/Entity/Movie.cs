﻿using System;
using System.Collections.Generic;

namespace DemoDatabaseFirst.Entity;

public partial class Movie
{
    public int MovieNo { get; set; }

    public string MovieName { get; set; } = null!;

    public double Duration { get; set; }

    public int Genre { get; set; }

    public string Director { get; set; } = null!;

    public decimal AmountOfMoney { get; set; }

    public string? Comment { get; set; }

    public string ImageLink { get; set; } = null!;

    public virtual ICollection<ActedIn> ActedIns { get; set; } = new List<ActedIn>();
}
