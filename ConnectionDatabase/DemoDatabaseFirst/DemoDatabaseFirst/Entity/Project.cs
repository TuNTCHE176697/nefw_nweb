﻿using System;
using System.Collections.Generic;

namespace DemoDatabaseFirst.Entity;

public partial class Project
{
    public int ProjectId { get; set; }

    public string ProjectName { get; set; } = null!;

    public DateTime ProjectStartDate { get; set; }

    public string ProjectDescription { get; set; } = null!;

    public string ProjectDetail { get; set; } = null!;

    public DateTime? ProjectCommpletedOn { get; set; }

    public virtual ICollection<ProjectModule> ProjectModules { get; set; } = new List<ProjectModule>();
}
