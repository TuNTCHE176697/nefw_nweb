﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Entity
{
    //[Table("Product")]

    public class Product
    {
        public Guid ProductId { get; set; }
  
        public string ProductName { get; set; }
        public Boolean IsAvailable { get; set; }
    }
}