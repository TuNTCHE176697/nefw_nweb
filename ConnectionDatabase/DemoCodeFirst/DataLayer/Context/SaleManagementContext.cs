﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DataLayer.Config;
using DataLayer.Entity;
using Microsoft.EntityFrameworkCore;

namespace DataLayer.Context
{
    public class SaleManagementContext : DbContext
    {
        public SaleManagementContext(DbContextOptions options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new ProductConfig());
        }

        //Trong CSDL DbContext có 1 bảng, mỗi dòng bảng đó là 1 phần tử tương ứng với kiểu Product và các thuộc tính của nó sẽ được dùng bởi Entity Framework
        public DbSet<Product> Products { get; set; }
    }
}