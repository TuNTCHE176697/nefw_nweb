﻿using BusinessLayer.IRepositories.Base;
using DataLayer.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.IRepositories
{
    public interface IProductRepository : IGenericRepository<Product>
    {
        public int GetNumberOfProduct();
        Task<IEnumerable<Product>> GetProducts();
        Task<Product> GetProduct(Guid productId);
        Task<Product> AddProduct(Product product);
        Task<Product> UpdateProduct(Product product);
        Task<Product> DeleteProduct(Guid productId);

    }
}
