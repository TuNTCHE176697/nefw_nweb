﻿using BusinessLayer.IRepositories;
using DataLayer.Entity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace DemoCodeFirst.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly IProductRepository _productRepository;

        public ProductController(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        [Route("api/GetNumberOfProduct")]
        [HttpGet]
        public int GetNumberOfProduct()
        {
            return _productRepository.GetNumberOfProduct();
        }


        [HttpGet("{id:Guid}")]
        public async Task<ActionResult<Product>> GetProduct(Guid id)
        {
            try
            {
                var result = await _productRepository.GetProduct(id);

                if (result == null)
                {
                    return NotFound();
                }

                return result;
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error retrieving data from the database");
            }
        }

        [Route("CreateProduct")]
        [HttpPost]
        public async Task<ActionResult<Product>> CreateProduct(Product product)
        {
            try
            {
                if (product == null)
                {
                    return BadRequest();
                }

                var createdProduct = await _productRepository.AddProduct(product);

                return CreatedAtAction(nameof(GetProduct), new { id = createdProduct.ProductId },
                    createdProduct);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error creating product");
            }
        }

        [HttpPut("{id:Guid}")]
        public async Task<ActionResult<Product>> UpdateProduct(Guid id, Product product)
        {
            try
            {
                if (id != product.ProductId)
                    return BadRequest("Employee ID mismatch");

                var productToUpdate = await _productRepository.GetProduct(id);

                if (productToUpdate == null)
                    return NotFound($"Employee with Id = {id} not found");

                return await _productRepository.UpdateProduct(product);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error updating data");
            }
        }

        [HttpDelete("{id:Guid}")]
        public async Task<ActionResult<Product>> DeleteProduct(Guid id)
        {
            try
            {
                var employeeToDelete = await _productRepository.GetProduct(id);

                if (employeeToDelete == null)
                {
                    return NotFound($"Product with Id = {id} not found");
                }

                return await _productRepository.DeleteProduct(id);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError,
                    "Error deleting data");
            }
        }
    }

}

